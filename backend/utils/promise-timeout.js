module.exports = (timeout = 1000) => {
    return new Promise(response => {
        setTimeout(response, timeout);
    });
};
