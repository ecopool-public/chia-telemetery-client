const router = require('express').Router();
const EcopoolController = require('../../controllers/ecopool-controller');
const localConfig = require('../../controllers/gui-controller/local-config');
const discoverCLI = require("../../controllers/coin-controller/discover-cli");

router.get('/currencies', async (req, res) => {
    const currenciesData = await EcopoolController.currencies();

    const currenciesDataToSend = currenciesData.answer
        .map((currency) => {
            const useThisCurrency = localConfig.get(`clientsSettings.${currency.appName}.use`);

            const CLIPath = localConfig.get(`clientsSettings.${currency.appName}.cliPath`) || discoverCLI(currency.appName);

            return {
                ...currency,
                settings: {
                    cliPath: CLIPath,
                    use: useThisCurrency == null ? false : useThisCurrency
                }
            }
        });

    res.send({
        success: true,
        answer: currenciesDataToSend
    })
});

module.exports = {router};
