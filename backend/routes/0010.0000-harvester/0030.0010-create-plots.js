const router = require('express').Router();
const createError = require('http-errors');
const ROUTES_ERROR_ANSWERS = require('../../constants/routes-error-answers');
const GuiController = require('../../controllers/gui-controller');

router.post('/create-plots', async (req, res, next) => {
    if (!GuiController.userIsLogged()) {
        return next(createError(401, ROUTES_ERROR_ANSWERS[401]));
    }

    const {
        body: {
            plotParams,
            madPlotter = false
        }
    } = req;
    const createPlotsStatus = GuiController.createPlots(plotParams, madPlotter);

    if (!createPlotsStatus) {
        return res.send({ success: false });
    }

    return res.send({ success: true });
});

module.exports = {
    needSocket: true,
    setSocket: (socketInstance) => {
        socket = socketInstance;
    },
    router
};
