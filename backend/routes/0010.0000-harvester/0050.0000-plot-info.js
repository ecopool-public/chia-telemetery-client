const router = require('express').Router();
const getPlotMemo = require('../../utils/get-plot-memo');

router.post('/get-plot-memo', async (req, res, next) => {
    const path = req.body.path;

    const memo = await getPlotMemo(path);

    return res.send({
        success: true,
        answer: memo
    });
});

module.exports = { router };
