const CoinController = require('../controllers/coin-controller');
const getLogs = require('../utils/get-logs');
const { debugLog } = getLogs('routes:io-client');

module.exports = {
    init (expressApp) {
        const io = expressApp.io;

        const applyIoRoutes = require('../io-routes');
        CoinController.logWatcher.start();

        io.on('connection', (socket) => {
            debugLog('Sockets successfully connected');

            socket.join('harvester-connection');
            socket.join('check-plots');
            socket.join('harvester-telemetry');

            applyIoRoutes(socket);
            CoinController.logWatcher.start();
        });
    }
};
