const router = require('express').Router();
const createError = require('http-errors');
const ROUTES_ERROR_ANSWERS = require('../../constants/routes-error-answers');
const GuiController = require('../../controllers/gui-controller');
const EcoPoolController = require('../../controllers/ecopool-controller');

router.get('/user/balance/:currencyId', async (req, res, next) => {
    if (!GuiController.userIsLogged()) {
        return next(createError(401, ROUTES_ERROR_ANSWERS[401]));
    }

    const {currencyId} = req.params;

    const requestData = await EcoPoolController.userBalance(currencyId);
    const { success, answer } = requestData;

    if (!success) {
        return res.send(requestData);
    }

    return res.send({
        success: true,
        answer: {
            amount: answer
        }
    });
});

module.exports = { router };
