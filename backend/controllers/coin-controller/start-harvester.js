const callCLIPromise = require('./call-cli-promise');
const getLogs = require('../../utils/get-logs');
const { debugLog, debugError } = getLogs('coin-controller:start-harvester');
const state = require('../../state');

module.exports = async (appName) => {
    const checkCommand = await callCLIPromise(appName, {
        commandPayload: {
            start: null,
            harvester: null,
            '-r': null
        },
        commandForStartHarvester: true
    });

    if (checkCommand.status) {
        state.harvesterIsWorking = true;
        debugLog(`Harvester successfully launched for ${appName}`);
    } else {
        debugError(`Failed to start harvester ${appName}`);
    }

    return checkCommand.status;
};
