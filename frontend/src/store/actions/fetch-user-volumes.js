import * as MutationsTypes from '../mutations/types';
import axios from "axios";

export default async (store) => {
    store.commit(MutationsTypes.SET_DASHBOARD_WIDGET_LOADING, {
        volumes: true
    });

    return axios.get(`/api/user/volume/${store.state.currencyId}?t=${Date.now()}`)
        .then(resp => {
            const respData = resp.data;
            if (!respData.success) {
                console.error('some error has occured', respData);
                return false;
            }
            const {
                answer: {
                    pool,
                    user,
                    totalPlotsCount
                }
            } = respData;

            store.commit(MutationsTypes.SET_DASHBOARD_WIDGET_VALUES, {
                poolNetSpace: Number(pool),
                userNetSpace: Number(user),
                userPlots: totalPlotsCount
            });
        })
        .catch(error => {
            console.error(error);
            // this.errors.netSpaces = true;
            store.commit(MutationsTypes.SET_DASHBOARD_WIDGET_ERROR, {
                netSpaces: true
            });
        })
        .finally(() => {
            store.commit(MutationsTypes.SET_DASHBOARD_WIDGET_LOADER, {
                poolNetSpace: false,
                userNetSpace: false
            });
            store.commit(MutationsTypes.SET_DASHBOARD_WIDGET_LOADING, {
                volumes: false
            });
        });
};
