const router = require('express').Router();
const { exec } = require('child_process');
const getLogs = require('../../utils/get-logs');
const { debugLog, debugError } = getLogs('routes:gui:restart');

router.get('/restart', async (req, res, next) => {
    debugLog('Process pm2 restart all started');
    res.send();

    // noinspection JSCheckFunctionSignatures
    exec('pm2 restart all', {
        detached: true
    }, (error, stdout, stderr) => {
        debugLog(stdout);

        if (stderr) {
            debugError(`Failed to restart gui. ${stderr}`);
        }
    });
    debugLog('Process pm2 restart all completed');
});

module.exports = { router };
