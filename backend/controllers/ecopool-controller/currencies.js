const ECO_POOL_API = require('../../constants/eco-pool-api');
const apiRequest = require('./api-request');
const getLogs = require('../../utils/get-logs');
const {debugLog, debugWarn, debugError} = getLogs('ecopool-controller:currencies');
const clearErrorData = require('../../utils/clear-error-data');

module.exports = async () => {
    let requestData;

    try {
        requestData = await apiRequest({
            url: ECO_POOL_API.currencies,
            method: 'get'
        });
    } catch (error) {
        debugError(JSON.stringify({
            message: `Unable to receive currencies`,
            error: clearErrorData(error)
        }));
        return {success: false};
    }


    return {
        success: true,
        answer: requestData.data
    };
};
