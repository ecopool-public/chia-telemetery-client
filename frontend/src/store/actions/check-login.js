import * as MutationsTypes from "../mutations/types";
import axios from "axios";
import router from "../../router";

export default async (store) => {
    try {
        let auth = await axios.get('/api/check-login');
        if (auth.data.success) {
            store.commit(MutationsTypes.SET_AUTH, true);
            router.push('/currencies-setup');
        } else {
            store.commit(MutationsTypes.SET_AUTH, false);
            store.commit(MutationsTypes.SET_PATH_STATUS, false);
        }
    } catch(e) {
        console.error(e);
    }
};