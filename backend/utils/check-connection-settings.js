module.exports = payload => {
    const {
        ip,
        port,
        debugError
    } = payload;

    if (!ip || !port) {
        debugError('Ip or port or both of the new connection is not forwarded');
        return false;
    } else {
        return true;
    }
};
