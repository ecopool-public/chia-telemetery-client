const EcopoolController = require("../../controllers/ecopool-controller");
const CoinController = require("../../controllers/coin-controller");

module.exports = async () => {
    const {answer: currencies} = await EcopoolController.currencies();
    return Promise.all(
        currencies.map(async (currencyItem) => {
            return {
                appName: currencyItem.appName,
                checkResult: await CoinController.checkCLI(currencyItem.appName)
            };
        })
    )
        .then((result) => {
            return result.reduce((res, curr) => {
                res[curr.appName] = curr.checkResult

                return res
            }, {})
        });
}