const getLogs = require('../../utils/get-logs');
const { debugLog, debugError } = getLogs('coin-controller:set-ratelimits');

const fs = require('fs');
const {homedir} = require('os');

const jsYaml = require('js-yaml');

module.exports = (appName) => {
    const pathToConfig = `${homedir()}/.${appName}/mainnet/config/config.yaml`;

    if (fs.existsSync(pathToConfig)) {
        let configData = fs.readFileSync(pathToConfig, 'utf8');

        try {
            const configObject = jsYaml.load(configData);
            configObject.outbound_rate_limit_percent = 1000;
            configObject.inbound_rate_limit_percent = 1000;
            configData = jsYaml.dump(configObject);

            fs.writeFileSync(pathToConfig, configData);

            debugLog(`config ratelimits for ${appName} successfully setted up`);
        } catch (e) {
            debugError(`error setup config ratelimits for ${appName}`, e);
        }
    } else {
        debugLog(`Config for ${appName} not found`);
    }
};
