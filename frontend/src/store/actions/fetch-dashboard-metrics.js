import * as ActionsTypes from './types';

export default async (store) => {
    return Promise.all([
        store.dispatch(ActionsTypes.FETCH_USER_VOLUMES),
        store.dispatch(ActionsTypes.FETCH_USER_BALANCE),
        store.dispatch(ActionsTypes.FETCH_USER_PENDING_WITHDRAWALS),
        store.dispatch(ActionsTypes.SUBSCRIBE_POINTS_INFO)
    ])
};
