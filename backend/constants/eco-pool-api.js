module.exports = {
    registration: '/registration',
    login: '/login',
    logout: '/logout',
    currencies: '/currencies'
};
