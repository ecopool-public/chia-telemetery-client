const axios = require('axios');
const config = require('config');
const state = require('../../state');
const getLogs = require('../../utils/get-logs');
const { debugLog } = getLogs('ecopool-controller:api-request');

module.exports = (payload) => {
    const {
        url,
        method = 'post',
        data = {}
    } = payload;
    const hasToken = Boolean(state.token);
    const preparedUrl = `${config.pool.host}${url}`;

    debugLog(`Start request to address ${preparedUrl}`);

    return axios({
        method,
        url: preparedUrl,
        data,
        headers: hasToken ? {
            Cookie: `connect.sid=${state.token}`
        } : {}
    })
        .finally(() => {
            debugLog(`Done request to address ${preparedUrl}`);
        });
};
