const checkConnectionSettings = require('../../../utils/check-connection-settings');
const CoinController = require('../../coin-controller');
const localConfig = require('../../gui-controller/local-config');
const compareConnections = require('../../../utils/compare-connections');
const getLogs = require('../../../utils/get-logs');
const { debugLog, debugWarn, debugError } = getLogs('ecopool-controller:sockets:setup-new-connection');
const state = require('../../../state');

module.exports = {
    init (socket) {
        socket.on('setup-new-connection', async (appName, payload) => {
            if (!payload) {
                debugError();
                return false;
            }

            const { farmerIp, farmerPort } = payload;
            const newConnectionData = {
                ip: farmerIp,
                port: farmerPort
            };
            const checkSettings = checkConnectionSettings({
                ...newConnectionData,
                debugError
            });

            if (!checkSettings) {
                return false;
            }

            if (state.init) {
                const saveConnectionData = localConfig.get('connection');
                const matchConnections = compareConnections(saveConnectionData, newConnectionData);

                if (matchConnections) {
                    debugWarn('New connection data and saved connection data matched');
                    return false;
                }
            }

            const setFarmerPeerStatus = await CoinController.setFarmerPeer(appName, newConnectionData);

            if (!setFarmerPeerStatus) {
                return false;
            }

            localConfig.set('connection', newConnectionData);
            debugLog(`New connection settings successfully installed for ${appName}`);
            return CoinController.restartHarvester(appName);
        });
    }
};
