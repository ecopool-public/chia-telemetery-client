import { createApp } from 'vue';
import { store } from './store';
import App from './App.vue';
import router from './router';
import axios from 'axios';
import VueAxios from 'vue-axios';
// ui-theme
import ElementPlus from 'element-plus';

import './assets/scss/tailwind.css';

// default settings for axios
axios.defaults.baseURL = `${window.location.protocol}//${window.location.hostname}:${CONFIG.port}`;

import byteSize from 'byte-size';
byteSize.defaultOptions({
    toString() {
        return `${this.value} ${this.unit}`
    }
});
// styles
import 'element-plus/lib/theme-chalk/index.css';
import './assets/scss/index.scss';

const app = createApp(App);

app.use(store);
app.use(router);
app.use(ElementPlus)
app.use(VueAxios, axios);
app.mount('#app');
