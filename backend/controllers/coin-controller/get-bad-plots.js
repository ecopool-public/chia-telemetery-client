const getHarvesterInstance = require('./harvester-instance');
const getLogs = require('../../utils/get-logs');
const { debugLog, debugWarn, debugError } = getLogs('coin-controller:get-bad-plots');
const state = require('../../state');

/**
 * @param {string} appName
 * @returns {Promise<boolean|*[]|string[]>}
 */
module.exports = async (appName) => {
    if (!state.harvesterIsWorking) {
        debugWarn('Harvester not started. Command could not be executed');
        return false;
    }

    const HarvesterInstance = await getHarvesterInstance(appName);
    let plotsData;

    try {
        plotsData = await HarvesterInstance.getPlots();
    } catch (error) {
        console.error(error);
        debugError(`getPlots method for ${appName} by HarvesterInstance failed`);
        return [];
    }

    if (!plotsData) {
        debugError(`getPlots method for ${appName} by HarvesterInstance return undefined`);
        return [];
    }

    if (!plotsData.success) {
        debugError(`getPlots method for ${appName} by HarvesterInstance success = false`);
        return [];
    }

    const badPlots = plotsData.not_found_filenames;

    if (!badPlots.length) {
        debugLog(`User has no invalid plots for ${appName}`);
        return [];
    }

    debugWarn(`Data on invalid plots for ${appName} successfully collected`);

    return badPlots;
};
