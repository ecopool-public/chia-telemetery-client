const callCLIPromise = require('./call-cli-promise');
const getLogs = require('../../utils/get-logs');
const { debugLog, debugError } = getLogs('coin-controller:set-farmer-peer');

/**
 *
 * @param appName
 * @param connectionString
 * @returns {Promise<*>}
 */
module.exports = async (appName, connectionString) => {
    const checkCommand = await callCLIPromise(appName, {
        commandPayload: {
            configure: null,
            '--set-farmer-peer': null,
            [connectionString]: null
        },
        commandForStartHarvester: true
    });

    if (checkCommand.status) {
        debugLog(`Successfully setup to: ${connectionString} for farmer peer for ${appName}`);
    } else {
        debugError(`Failed setup to: ${connectionString} for farmer peer for ${appName}`);
    }

    return checkCommand.status;
};
