const router = require('express').Router();
const EcoPoolController = require('../../controllers/ecopool-controller');
const CoinController = require("../../controllers/coin-controller");
// const GuiController = require('../../controllers/gui-controller');

router.post('/login', async (req, res) => {
    const { body: { login, password } } = req;
    const requestData = await EcoPoolController.userLogin({
        login,
        password
    });

    if (!requestData.success) {
        return res.send(requestData);
    }

    const checkClients = await CoinController.checkAllClients();

    const answer = {
        success: true,
        answer: {
            checkClients
        }
    };

    return res.send(answer);
});

module.exports = { router };
