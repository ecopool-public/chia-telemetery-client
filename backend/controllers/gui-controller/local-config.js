const config = require('config');
const path = require('path');
const fs = require('fs');
const getLogs = require('../../utils/get-logs');
const { debugLog, debugWarn, debugError } = getLogs('gui-controller:local-config');

const pathToLocalConfig = path.join(process.cwd(), './config/local.json');

(() => {
    if (fs.existsSync(pathToLocalConfig)) {
        try {
            const data = fs.readFileSync(pathToLocalConfig);
            const parsedConfig = JSON.parse(data);

            if (parsedConfig.hasOwnProperty('chiaCliPath')) {
                parsedConfig.clientsSettings = {
                    'chia': {
                        'cliPath': parsedConfig.chiaCliPath,
                        'use': true
                    }
                };

                delete parsedConfig.chiaCliPath;

                fs.writeFileSync(pathToLocalConfig, JSON.stringify(parsedConfig, null, 2));
            }
        } catch(e) {
            debugLog('Error check config version');
        }
    }
})();

const set = (key, value) => {
    try {
        const data = fs.readFileSync(pathToLocalConfig);
        const parsedConfig = JSON.parse(data);

        const splittedKey = key.split('.');
        splittedKey
            .reduce((res, keyPart, idx) => {
                if (idx === splittedKey.length - 1) {
                    res = res[keyPart] = value;
                } else {
                    res = res[keyPart] = res[keyPart] ? res[keyPart] : {};
                }

                return res;
            }, parsedConfig);

        // parsedConfig[key] = value;
        fs.writeFileSync(pathToLocalConfig, JSON.stringify(parsedConfig, null, 2));
        debugLog(`Key: ${key} with value: ${value} in local.json updated successfully`);
    } catch (error) {
        fs.writeFileSync(pathToLocalConfig, JSON.stringify({
            [key]: value
        }));
        debugLog(`local.json was created successfully and key: ${key} with value: ${value} added successfully`);
    }

    config[key] = value;
};
const get = (key) => {
    try {
        const data = fs.readFileSync(pathToLocalConfig);
        const parsedConfig = JSON.parse(data);

        const splittedKey = key.split('.');

        const result = splittedKey
            .reduce((res, key) => {
                if (!res) {
                    return res;
                }

                return res[key];
            }, parsedConfig);

        if (result !== undefined) {
            return result;
        } else {
            debugWarn(`Key: ${key} not found in local.json`);
            return null;
        }
    } catch (error) {
        debugError(JSON.stringify({
            message: 'Failed to read local.json',
            error
        }));
    }
};

const remove = (key) => {
    try {
        const data = fs.readFileSync(pathToLocalConfig);
        const parsedConfig = JSON.parse(data);
        const hasKey = Boolean(parsedConfig[key]);

        if (hasKey) {
            delete parsedConfig[key];
            fs.writeFileSync(pathToLocalConfig, JSON.stringify(parsedConfig, null, 2));
            debugLog(`Key: ${key} in local.json deleted successfully`);
        } else {
            debugWarn(`Key: ${key} not found in local.json`);
        }
    } catch (error) {
        debugError(JSON.stringify({
            message: 'Failed to read local.json',
            error
        }));
    }
};

module.exports = {
    set,
    get,
    remove
};
