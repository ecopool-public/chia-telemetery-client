import * as MutationsTypes from '../mutations/types';
import axios from "axios";

export default async (store) => {
    store.commit(MutationsTypes.SET_LOADING, true);

    let tableDirectories;
    let resp = await axios.get(`/api/harvester/get-plots-directories/${store.state.appName}`);
    if (resp.data.success) {
        tableDirectories = resp.data.answer.directories.map(item => ({ path: item }));
    } else {
        tableDirectories = [];
    }

    store.commit(MutationsTypes.PLOT_DIRECTORIES_SET, tableDirectories);
    store.commit(MutationsTypes.SET_LOADING, false);
};
