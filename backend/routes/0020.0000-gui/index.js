const fs = require('fs');
const router = require('express').Router();

fs.readdirSync(__dirname)
    .sort((a, b) => {
        if (a < b) {
            return -1;
        }

        if (a > b) {
            return 1;
        }

        return 0;
    })
    .filter((item) => item !== 'index.js')
    .forEach(fileName => {
        const routerModule = require(`./${fileName}`);
        router.use(routerModule.router);
    });

module.exports = {
    basePath: '/api/gui',
    router
};
