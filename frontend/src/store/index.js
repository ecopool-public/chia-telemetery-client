import {createStore} from 'vuex';

import getters from './getters';
import mutations from './mutations';
import actions from './actions';
import packageSrc from "../../../package.json";

export const store = createStore({
    getters,
    mutations,
    actions,
    state: {
        pathStatus: false,
        isAuth: false,
        newVersion: packageSrc.version,
        blockVersion: false,
        balance: 0,
        pendingWithdrawals: [],
        ID: '',
        currencyId: localStorage.getItem('currencyId') || 'xch',
        appName: localStorage.getItem('appName') ||'chia',
        currencyPrice: 0,
        loading: null,
        loadingsParts: {
            volumes: false,
            balance: false,
            pendingWithdrawal: false
        },
        dashboardWidgets: {
            loadings: {
                volumes: false,
                balance: false,
                pendingWithdrawal: false
            },
            poolNetSpace: '0 GiB',
            userNetSpace: '0 GiB',
            userPlots: 0,
            loaders: {
                userNetSpace: true,
                userBalance: true,
                poolNetSpace: true,
                pendingWithdrawal: true
            },
            fetchInterval: null,
            errors: {
                netSpaces: false,
                userBalance: false,
                pendingWithdrawal: false
            }
        },
        plotDirectories: {
            tableDirectories: []
        },
        farmData: {
            plots: [],
            badPlots: []
        },
        connectionState: {
            loading: true,
            ping: {
                timeout: null,
                value: 0,
            },
            quality: null,
            status: null,
            timeout: null
        },
        harvesterProofsTable: {},
        harvesterChartInfo: {},
        harvesterId: null,
        harvestersDetails: [],
        currencies: {
            list: []
        }
    }
});
