const discoverCLI = require('./discover-cli');
const callCLIPromise = require('./call-cli-promise');
const GuiController = require('../gui-controller');
const getLogs = require('../../utils/get-logs');
const { debugLog, debugError } = getLogs('coin-controller:check-cli');

/**
 *
 * @param {string} appName
 * @param {string} customPath
 * @returns {Promise<*>}
 */
module.exports = async (appName, customPath = null) => {
    const CLIPath = customPath ? customPath : discoverCLI(appName);

    if (!CLIPath) {
        return false;
    }

    const checkCLICommand = await callCLIPromise(appName, {
        commandPayload: {
            '-h': null
        },
        customCLIPath: CLIPath,
        commandForStartHarvester: true
    });

    if (checkCLICommand.status) {
        GuiController.localConfig.set(`clientsSettings.${appName}.cliPath`, CLIPath);
        debugLog(`Path to ${appName}-blockchain found successfully`);
    } else {
        debugError(`Path to ${appName}-blockchain not found`);
    }

    return checkCLICommand.status;
};
