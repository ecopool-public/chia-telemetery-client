import * as MutationsTypes from "../mutations/types";
import axios from "axios";

export default async (store) => {
    axios.get('/api/gui/get-version')
        .then(resp => {
            const {
                data: { success }
            } = resp;

            if (!success) {
                return false;
            }

            const {
                data: {
                    answer: {
                        version,
                        blockVersion
                    }
                }
            } = resp;

            store.commit(MutationsTypes.SET_NEW_VERSION, version);
            store.commit(MutationsTypes.SET_BLOCK_VERSION, blockVersion);
        })
        .catch(error => {
            console.error(error);
        });
};
