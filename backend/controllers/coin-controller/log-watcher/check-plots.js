const getCheckPlotsDataFromLog = require('../../../utils/get-check-plots-data-from-log');
const getLogs = require('../../../utils/get-logs');
const { debugLog } = getLogs('coin-controller:log-watcher:check-plots');
const state = require('../../../state');
const {io} = require('../../../app');

let lastTime = null;

module.exports = (appName, lastLogString) => {
    const plotsData = getCheckPlotsDataFromLog(lastLogString);

    if (!plotsData) {
        return false;
    }

    const time = new Date().getTime();

    if (
        time === lastTime ||
        time - lastTime < 1000
    ) {
        return false;
    }

    const checkPlotsItem = {
        time: plotsData.time,
        timestamp: time
    };

    state.histories.checkPlots[appName].unshift(checkPlotsItem);
    state.histories.checkPlots[appName] = state.histories.checkPlots[appName].slice(0, 200);
    lastTime = time;

    debugLog('Data about plots checking sent to client');

    io.to('check-plots').emit('check-plots', appName, checkPlotsItem);
};
