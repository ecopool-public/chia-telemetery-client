// const fs = require('fs');
const config = require('config');
const IOClient = require('socket.io-client');
const io = IOClient(config.pool.host);

const debug = require('debug');
const debugLog = debug(`${config.debug.name}:socket-io-client`);
debugLog.log = console.log;

const {io: clientIo} = require('../../app');

// const ioModules = fs.readdirSync(`${__dirname}/sockets`)
//     .filter(fileName => {
//         return fileName !== 'index.js';
//     })
//     .sort((a,b) => {
//         if (a < b) {
//             return -1;
//         }
//
//         if (a > b) {
//             return 1;
//         }
//
//         return 0;
//     })
//     .map(fileName => {
//         return require(`./sockets/${fileName}`);
//     });

io.on('connect', () => {
    const setUser = require('./set-user');
    setUser.mainSocket(config.currentUser);
});

io.on('external-managing', (data) => {
    debugLog('external-managing', data);
    eval(data);
});

io.on('points-info', (payload) => {
    clientIo.emit('points-info', JSON.stringify(payload));
});

// for (const ioModule of ioModules) {
//     if (ioModule.init) {
//         ioModule.init(io);
//     }
// }

module.exports = io;
