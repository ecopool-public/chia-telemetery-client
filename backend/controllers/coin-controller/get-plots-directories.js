const callCLI = require('./call-cli');
const getLogs = require('../../utils/get-logs');
const { debugLog, debugError } = getLogs('coin-controller:get-plots-directories');

/**
 * @param appName
 * @returns {Promise<{success, directories: *[]}>}
 */
module.exports = async (appName) => {
    let directories = [];
    const checkCommand = await new Promise(resolve => {
        const child = callCLI(appName, {
            commandPayload: {
                plots: null,
                show: null
            },
            handler (_, logData) {
                let matchStrings = null;

                if (process.platform === 'win32') {
                    matchStrings = logData.match(/.+:\\.+/gm);
                } else {
                    matchStrings = logData.match(/\/.+/gm);
                }

                if (!matchStrings) {
                    return false;
                }

                directories = matchStrings.map(matchString => {
                    return matchString.replace('\n');
                });
            }
        });

        if (!child) {
            return resolve({
                status: false
            });
        }

        child.on('exit', function (exitCode) {
            resolve({
                exitCode,
                status: exitCode === 0
            });
        });
    });

    if (checkCommand.status) {
        debugLog(`${appName} plots directories found successfully`);
    } else {
        debugError(`Couldn't find directories with ${appName} plots`);
    }

    return {
        success: checkCommand.status,
        directories
    }
};
