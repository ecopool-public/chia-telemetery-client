const path = require('path');
const callCLIPromise = require('./call-cli-promise');
const getLogs = require('../../utils/get-logs');
const { debugLog, debugError } = getLogs('coin-controller:set-certificates');

module.exports = async (appName) => {
    const certificatesPath = path.join(process.cwd(), '/pool-certificates', `/${appName}`);
    if (appName === 'chia') {
        await callCLIPromise(appName, {
            commandPayload: {
                init: null,
                '--fix-ssl-permissions': null
            },
            commandForStartHarvester: true
        });
    } else {
        await callCLIPromise(appName, {
            commandPayload: {
                init: null
            },
            commandForStartHarvester: true
        });
    }

    const checkCommand = await callCLIPromise(appName, {
        commandPayload: {
            init: null,
            '-c': null,
            [certificatesPath]: null
        },
        commandForStartHarvester: true
    });

    await callCLIPromise(appName, {
        commandPayload: {
            init: null,
            '--fix-ssl-permissions': null
        },
        commandForStartHarvester: true
    });

    if (checkCommand.status) {
        debugLog(`Pool certificates for ${appName} installed successfully`);
    } else {
        debugError(`Failed to install pool certificates for ${appName}`);
    }

    return checkCommand.status;
};
