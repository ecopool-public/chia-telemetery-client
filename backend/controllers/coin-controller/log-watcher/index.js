const fs = require('fs');
const path = require('path');
const { homedir } = require('os');
const Tail = require('tail').Tail;
const getLogs = require('../../../utils/get-logs');
const { debugLog, debugWarn, debugError } = getLogs('coin-controller:log-watcher');

const EcopoolController = require('../../ecopool-controller');

const lastOnline = require('./last-online');
const checkPlots = require('./check-plots');
const harvesterTelemetry = require('./harvester-telemetry');
const state = require("../../../state");

const STARTING_STATES = {};
const TAIL_INSTANCES = {};

const getPathToLogFile = (appName) => {
    return path.join(homedir(), `.${appName}`, 'mainnet', 'log', 'debug.log');
};

const startCoinWatcher = (appName) => {
    const starting = !!STARTING_STATES[appName];
    if (starting) {
        debugWarn(`Reading ${appName} logs is already running, you can only restart`);
        return false;
    }

    const pathToLogFile = getPathToLogFile(appName);

    try {
        if (!fs.existsSync(pathToLogFile)) {
            fs.writeFileSync(pathToLogFile, '');
        }
    } catch(e) {
        debugWarn('Error check log path for', appName, pathToLogFile, e);
        STARTING_STATES[appName] = false;
        return false;
    }

    TAIL_INSTANCES[appName] = new Tail(pathToLogFile);

    if (!state.histories.farmingPlots[appName]) {
        state.histories.farmingPlots[appName] = [];
    }

    if (!state.histories.proofs[appName]) {
        state.histories.proofs[appName] = [];
    }

    if (!state.histories.checkPlots[appName]) {
        state.histories.checkPlots[appName] = [];
    }

    TAIL_INSTANCES[appName].on('line', data => {
        lastOnline(appName, data);
        checkPlots(appName, data);
        harvesterTelemetry(appName, data);
    });

    TAIL_INSTANCES[appName].on('error', error => {
        debugError(JSON.stringify({
            message: `An error occured while reading ${appName} logs`,
            error
        }));
        setTimeout(() => {
            restart(appName);
        }, 3000);
    });

    STARTING_STATES[appName] = true;
    debugLog(`Reading ${appName} logs started`);
};

const stopCoinWatcher = (appName) => {
    const pathToLogFile = getPathToLogFile(appName);

    if (!fs.existsSync(pathToLogFile)) {
        return false;
    }

    TAIL_INSTANCES[appName].unwatch();
    STARTING_STATES[appName] = false;
    debugLog(`Reading ${appName} logs stopped`);
};

const restartCoinWatcher = (appName) => {
    const pathToLogFile = getPathToLogFile(appName);

    if (!fs.existsSync(pathToLogFile)) {
        setTimeout(() => {
            restartCoinWatcher(appName);
        }, 3000);
        return false;
    }

    stopCoinWatcher(appName);
    startCoinWatcher(appName);
};

const executeFuncForAllCurrencies = (cb) => {
    return EcopoolController.currencies()
        .then((response) => {
            const {answer: currencies} = response;

            return Promise.all(
                currencies.map((currencyInfo) => {
                    return cb(currencyInfo.appName);
                })
            )
        });
};

const start = async (appName) => {
    if (appName) {
        return startCoinWatcher(appName);
    }

    return executeFuncForAllCurrencies(startCoinWatcher);
};

const stop = async (appName) => {
    if (appName) {
        return stopCoinWatcher(appName);
    }

    return executeFuncForAllCurrencies(stopCoinWatcher);
};

const restart = async (appName) => {
    if (appName) {
        return restartCoinWatcher(appName);
    }

    return executeFuncForAllCurrencies(restartCoinWatcher);
};

module.exports = {
    start,
    restart,
    stop
};
