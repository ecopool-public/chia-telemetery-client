const callCLI = require('./call-cli');

/**
 *
 * @param {string} appName
 * @param {{}} payload
 * @returns {Promise<unknown>}
 */
module.exports = (appName, payload) => {
    return new Promise(resolve => {
        const child = callCLI(appName, payload);

        child.on('exit', function (exitCode) {
            resolve({
                exitCode,
                status: exitCode === 0
            });
        });
    });
};
