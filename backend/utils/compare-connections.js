module.exports = (connectionDataA, connectionDataB) => {
    return connectionDataA.ip === connectionDataB.ip && connectionDataA.port === connectionDataB.port;
};
