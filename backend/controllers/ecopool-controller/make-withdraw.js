const socketIoClient = require('./socket-io-client');
const getLogs = require('../../utils/get-logs');
const { debugLog, debugError } = getLogs('ecopool-controller:make-withdraw');

module.exports = async (payload, password) => {
    let requestData;

    try {
        requestData = await new Promise(resolve => {
            socketIoClient.emit('create-withdrawal-request', payload, password, resolve);
        });
    } catch (error) {
        debugError(JSON.stringify({
            message: 'Failed to execute withdrawal request',
            error
        }));
        return { success: false };
    }

    if (!requestData.success) {
        debugError('Failed to execute output');
        return requestData;
    }

    debugLog('Withdrawal completed successfully');

    return {
        success: true,
        answer: requestData
    };
};
