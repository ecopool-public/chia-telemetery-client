export default (state, keyValue) => {
    Object.keys(keyValue)
        .forEach((key) => {
            state.dashboardWidgets.errors[key] = keyValue[key];
        });
};