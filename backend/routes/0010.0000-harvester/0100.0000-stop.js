const router = require('express').Router();
const createError = require('http-errors');
const ROUTES_ERROR_ANSWERS = require('../../constants/routes-error-answers');
const GuiController = require('../../controllers/gui-controller');
const CoinController = require('../../controllers/coin-controller');
const getLogs = require('../../utils/get-logs');
const { debugLog, debugError } = getLogs('routes:harvester:restart');

router.get('/stop/:appName', async (req, res, next) => {
    if (!GuiController.userIsLogged()) {
        return next(createError(401, ROUTES_ERROR_ANSWERS[401]));
    }

    const {appName} = req.params;
    GuiController.localConfig.set(`clientsSettings.${appName}.use`, false);

    debugLog('Stopping harvester', appName);

    const stopHarvesterStatus = await CoinController.stopAllProcess(appName);

    if (!stopHarvesterStatus) {
        debugError(`An error occurred while stopping the harvester for ${appName}`);

        return res.send({
            success: false
        });
    }

    debugLog(`Harvester for ${appName} stopped`);

    return res.send({
        success: true
    });
});

module.exports = { router };
