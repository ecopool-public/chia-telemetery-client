const EcoPoolController = require('../controllers/ecopool-controller');
const getLogs = require('../utils/get-logs');
const { debugLog } = getLogs('io-routes:new-mnemonics');

module.exports = {
    init (socket) {
        EcoPoolController.socketIoClient.on('new-mnemonics', payload => {
            const resp = {
                success: Boolean(payload),
                answer: payload ? payload : 'error'
            };

            debugLog('Resend mnemonic from server to frontend');
            socket.emit('new-mnemonics', resp);
        });
    }
};
