const getPlotMemo = require('../../utils/get-plot-memo');
const getPreparedPlotDataForServer = require('../../utils/get-prepared-plot-data-for-server');
const getHarvesterInstance = require('./harvester-instance');
const getLogs = require('../../utils/get-logs');
const { debugLog, debugWarn, debugError } = getLogs('coin-controller:get-plots');
const state = require('../../state');

module.exports = async (appName, forClient = false) => {
    if (!state.harvesterIsWorking) {
        debugWarn(`Harvester for ${appName} not started. Command could not be executed`);
        return false;
    }

    let plotsData;

    const HarvesterInstance = await getHarvesterInstance(appName);

    try {
        plotsData = await HarvesterInstance.getPlots();
    } catch (error) {
        console.error(error);
        debugError(`getPlots method for ${appName} by HarvesterInstance failed`);
        return [];
    }

    if (!plotsData) {
        debugError(`getPlots method for ${appName} by HarvesterInstance return undefined`);
        return []
    }

    if (!plotsData.success) {
        debugError(`getPlots method for ${appName} by HarvesterInstance success = false`);
        return [];
    }

    const plots = plotsData.plots;

    if (!plots.length) {
        debugLog(`User has no valid plots for ${appName}`);
        return [];
    }

    const preparedPlots = [];

    for (const plot of plots) {
        preparedPlots.push({
            ...getPreparedPlotDataForServer(plot, forClient),
            memo: forClient ? undefined : await getPlotMemo(plot.filename)
        });
    }

    debugLog(`Data on valid plots successfully collected for ${appName}`);

    return preparedPlots;
};
