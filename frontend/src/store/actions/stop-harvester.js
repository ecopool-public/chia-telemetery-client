import * as MutationsTypes from '../mutations/types';
import * as ActionsTypes from './types';
import axios from "axios";
import {ElNotification} from "element-plus";

export default async (store, appName) => {
    store.commit(MutationsTypes.SET_LOADING, {
        state: true,
        text: `Stopping harvester for ${appName}`
    });

    axios.get(`/api/harvester/stop/${appName}`)
        .then((response) => {
            if (response.data.success) {
                ElNotification.success('Harvester successfully stopped');
            } else {
                ElNotification.error(`Error stopping harvester`);
            }
        })
        .finally(() => {
            store.commit(MutationsTypes.SET_LOADING, false);
            return store.dispatch(ActionsTypes.FETCH_CURRENCIES_DATA);
        });
}