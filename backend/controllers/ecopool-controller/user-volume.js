const syncSocketRequest = require('./sync-socket-request');
const getLogs = require('../../utils/get-logs');
const { debugLog, debugError } = getLogs('ecopool-controller:user-volume');

module.exports = async (currencyId) => {
    let requestData;

    try {
        requestData = await syncSocketRequest('my-volume', currencyId);
    } catch (error) {
        debugError(JSON.stringify({
            message: 'Failed to get user capacity data from server',
            error
        }));
        return { success: false };
    }

    debugLog('User power data received from the server');

    return {
        success: true,
        answer: requestData
    };
};
