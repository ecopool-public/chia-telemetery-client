const router = require('express').Router();
const createError = require('http-errors');
const ROUTES_ERROR_ANSWERS = require('../../constants/routes-error-answers');
const GuiController = require('../../controllers/gui-controller');
const getLogs = require('../../utils/get-logs');
const { debugLog } = getLogs('routes:harvester:get-telemetry-history');
const state = require('../../state');

router.get('/get-telemetry-history', async (req, res, next) => {
    if (!GuiController.userIsLogged()) {
        return next(createError(401, ROUTES_ERROR_ANSWERS[401]));
    }

    debugLog('The history of the plots that passed the filter has been sent to the client');

    return res.send({
        success: true,
        answer: {
            history: state.histories.farmingPlots
        }
    });
});

module.exports = { router };
