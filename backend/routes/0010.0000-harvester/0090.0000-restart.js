const router = require('express').Router();
const createError = require('http-errors');
const ROUTES_ERROR_ANSWERS = require('../../constants/routes-error-answers');
const GuiController = require('../../controllers/gui-controller');
const CoinController = require('../../controllers/coin-controller');
const getLogs = require('../../utils/get-logs');
const { debugLog, debugError } = getLogs('routes:harvester:restart');

router.get('/restart/:appName', async (req, res, next) => {
    if (!GuiController.userIsLogged()) {
        return next(createError(401, ROUTES_ERROR_ANSWERS[401]));
    }

    const {appName} = req.params;
    GuiController.localConfig.set(`clientsSettings.${appName}.use`, true);

    debugLog('Harvester restart started');

    const restartHarvesterStatus = await CoinController.startHarvester(appName);

    if (!restartHarvesterStatus) {
        debugError(`An error occurred while restarting the harvester for ${appName}`);

        return res.send({
            success: false
        });
    }

    debugLog(`Harvester for ${appName} restarted`);

    return res.send({
        success: true
    });
});

module.exports = { router };
