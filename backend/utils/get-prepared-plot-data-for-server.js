const camelCase = require('camelcase');
const getFileNameAndPath = require('./get-file-name-and-path');
const excludeKeysForClient = [
    'plot-seed',
    'plot_public_key',
    'pool_contract_puzzle_hash',
    'pool_public_key',
    'time_modified'
];

module.exports = (rawObject, forClient = false) => {
    const objectKeys = Object.keys(rawObject);

    return objectKeys.reduce((acc, key) => {
        if (key === 'filename') {
            const data = getFileNameAndPath(rawObject[key]);

            acc[key] = data.fileName;

            if (forClient) {
                acc.path = data.path;
            }
        } else {
            if (forClient && !excludeKeysForClient.includes(key)) {
                acc[camelCase(key)] = rawObject[key];
            } else {
                acc[camelCase(key)] = rawObject[key];
            }
        }

        return acc;
    }, {});
};
