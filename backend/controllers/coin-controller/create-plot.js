const config = require('config');
const PLOTS_SIZES = require('../../constants/plots-sizes');
const DEFAULT_PLOTS_RAM_SIZES = require('../../constants/default-plots-ram-sizes');
const callCLI = require('./call-cli');
const getLogs = require('../../utils/get-logs');
const { debugLog, debugWarn, debugError } = getLogs('coin-controller:create-plot');
const poolKeys = config.poolKeys;

/**
 * @param {string} appName
 * @param {{}} payload
 * @returns {ChildProcessWithoutNullStreams|boolean}
 */
module.exports = (appName, payload) => {
    let {
        size,
        ram,
        threads,
        buckets,
        bitField = false,
        tempFolder,
        tempFolder2,
        persistentFolder
    } = payload;

    if (!tempFolder) {
        debugError('Temporary folder not transferred');
        return false;
    }

    if (!persistentFolder) {
        debugError('Persistent folder not transferred');
        return false;
    }

    if (!PLOTS_SIZES.includes(size)) {
        debugWarn('The size of the plot is transmitted incorrectly, the size is set to k32 by default');
        size = 32;
    }

    if (!ram) {
        debugWarn(`The size of the RAM was not transferred, ${DEFAULT_PLOTS_RAM_SIZES[32]} MiB is set by default`);
        ram = DEFAULT_PLOTS_RAM_SIZES[size] || DEFAULT_PLOTS_RAM_SIZES[32];
    }

    if (!threads) {
        debugWarn('The number of threads is not transmitted, the default is 2');
        threads = 2;
    }

    if (!buckets) {
        debugWarn('The number of baskets has not been transmitted, the default is 128');
        buckets = 128;
    }

    const localPoolKeys = poolKeys[appName === 'chives' ? 'xcc' : 'xch'];

    const plotParams = {
        plots: null,
        create: null,
        '-k': size,
        '-b': ram,
        '-r': threads,
        '-u': buckets,
        '-n': 1,
        '-t': tempFolder,
        '-d': persistentFolder,
        '-f': localPoolKeys.farmer,
        '-p': localPoolKeys.pool
    };

    if (bitField) {
        plotParams['-e'] = null;
    }

    if (tempFolder2) {
        plotParams['-2'] = tempFolder2;
    }

    const stringPlotParams = `size: k${size}, ram: ${ram}, threads: ${threads}, buckets: ${buckets}, bitField: ${bitField}, tempFolder: ${tempFolder}, ${tempFolder2 ? `tempFolder2: ${tempFolder2}, ` : ''} persistentFolder: ${persistentFolder}`;

    debugLog(`Creation plot with parameters: (${stringPlotParams}) started`);

    return callCLI(appName, {
        commandPayload: plotParams,
        commandForStartHarvester: true
    });
};
