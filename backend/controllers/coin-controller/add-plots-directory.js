const getHarvesterInstance = require('./harvester-instance');
const getLogs = require('../../utils/get-logs');
const { debugLog, debugWarn, debugError } = getLogs('coin-controller:add-plots-directory');
const state = require('../../state');

/**
 *
 * @param {string} appName
 * @param {string} directory
 * @returns {Promise<boolean|*>}
 */
module.exports = async (appName, directory) => {
    if (!directory) {
        debugError('No plots directory specified');
        return false;
    }

    if (!state.harvesterIsWorking) {
        debugWarn('Harvester not started. Command could not be executed');
        return false;
    }

    const HarvesterInstance = await getHarvesterInstance(appName);

    const { success } = await HarvesterInstance.addPlotDirectory(directory);

    if (success) {
        debugLog(`Directory ${directory} with plots successfully added for ${appName}`);
    } else {
        debugError(`Failed to add directory ${directory} with plots for ${appName}`);
    }

    return success;
};
