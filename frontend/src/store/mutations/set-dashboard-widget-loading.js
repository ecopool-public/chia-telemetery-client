export default (state, keyValue) => {
    Object.keys(keyValue)
        .forEach((key) => {
            state.dashboardWidgets.loadings[key] = keyValue[key];
        });
};