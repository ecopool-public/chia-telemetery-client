import {ElLoading} from "element-plus";

export default (state, loadingState) => {
    let loadingText = 'Loading';
    let stateToSet;

    if (typeof loadingState === 'boolean') {
        stateToSet = loadingState;
    } else {
        loadingText = loadingState.text;
        stateToSet = loadingState.state;
    }

    if (!stateToSet && state.loading) {
        state.loading.close();
        state.loading = null;
    } else if (stateToSet && !state.loading) {
        state.loading = ElLoading.service({
            lock: true,
            text: loadingText,
            spinner: 'el-icon-loading',
            background: 'rgba(0, 0, 0, 0.7)'
        });
    }
};