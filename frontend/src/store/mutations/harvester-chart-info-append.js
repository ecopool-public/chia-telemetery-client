export default (state, {appName, ...payload}) => {
    const collection = state.harvesterChartInfo[appName];

    while (collection.length > 200) {
        collection.shift();
    }

    collection.push(payload.filter);
};