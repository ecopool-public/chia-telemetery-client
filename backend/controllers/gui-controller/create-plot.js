const checkParallelPlotting = require('../../utils/check-parallel-plotting');
const checkPlottingInQueue = require('../../utils/check-plotting-in-queue');
const createDefaultPlot = require('../coin-controller/create-plot');
const createMadPlot = require('../mad-max-plotter-controller/create-plot');
const createTestPlot = require('../coin-controller/create-test-plot');
const getLogs = require('../../utils/get-logs');
const { debugLog, debugWarn, debugError } = getLogs('gui-controller:create-plot');
const state = require('../../state');

const {io} = require('../../app');

const createPlot = async (queue, force = false) => {
    if (!queue) {
        debugError('Queue name not passed');
        return false;
    }

    const queueData = state.queues[queue];

    if (!queueData) {
        debugError(`Queue data ${queue} not found`);
        return false;
    }

    const isParallelPlotting = checkParallelPlotting(queueData);
    const queueLength = queueData.length;

    if (isParallelPlotting) {
        for (const plotData of queueData) {
            if (!plotData.starting) {
                setTimeout(() => {
                    startPlotting({
                        queueData,
                        queue,
                        plotData
                    });
                    io.emit('plot-complete');
                }, plotData.parallel.delay);
            }
        }
    } else {
        const queueIsPlotting = checkPlottingInQueue(queueData);

        if (queueIsPlotting && !force) {
            debugWarn(`Plotting in queue ${queue} is already running`);
            return;
        }

        const plotData = force ? queueData[queueLength - 2] : queueData[queueLength - 1];

        startPlotting({
            queueData,
            queue,
            plotData
        });
    }
};
const sendLogStringAndSaveInHistory = (string, plotData) => {
    const logData = {
        timestamp: Date.now(),
        string
    };

    plotData.logs.push(logData);
    io.emit(`plot-${plotData.id}`, logData);
};

const startPlotting = (payload) => {
    const {
        queueData,
        plotData,
        queue
    } = payload;
    const isParallel = plotData.parallel.active;

    plotData.starting = true;

    // if (process.env.NODE_ENV === 'development') {
    //     plotData.process = createTestPlot(plotData.options.tempFolder, plotData.options.persistentFolder);
    // } else {
        if (plotData.madPlotter) {
            plotData.process = createMadPlot(plotData.options);
        } else {
            const {size} = plotData.options;
            const appNameToCreate = size < 32 ? 'chives' : 'chia';
            plotData.process = createDefaultPlot(appNameToCreate, plotData.options);
        }
    // }

    const plotProcess = plotData.process;

    debugLog(`Plotting in queue ${queue} started successfully`);

    plotProcess.stdout.on('data', (data) => {
        const logString = data.toString().trim();
        const isFinishCompilePlot = logString.includes('Time for phase 4') || logString.includes('Total plot creation time was');

        if (isFinishCompilePlot) {
            if (!isParallel) {
                createPlot(queue, true);
            }
        }

        sendLogStringAndSaveInHistory(logString, plotData);
    });

    plotProcess.stderr.on('data', (data) => {
        sendLogStringAndSaveInHistory(data.toString().trim(), plotData);
    });

    plotProcess.on('exit', (exitCode) => {
        if (exitCode === 0) {
            if (!isParallel) {
                queueData.pop();
            } else {
                const plotIndex = queueData.findIndex($plotData => {
                    return $plotData.id === plotData.id;
                });
                queueData.splice(plotIndex, 1);
            }

            debugLog(`Plotting in queue ${queue} completed successfully`);

            if (!queueData.length) {
                delete state.queues[queue];
                debugLog(`Queue ${queue} completed successfully`);
            }
        } else if (exitCode !== null) {
            plotData.logs.push({
                timestamp: Date.now(),
                string: `Error code ${exitCode}`
            });
            debugError(`Plotting on queue ${queue} failed`);
        }

        io.emit('plot-complete');
    });
};

module.exports = createPlot;
