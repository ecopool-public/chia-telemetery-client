import socket from "../../sockets";
import * as MutationsTypes from '../mutations/types';

const TIMEOUT_DELAY = 1000 * 60 * 2; // 2 минуты
let CONNECTION_INITIALIZED = false;

export default async (store) => {
    if (CONNECTION_INITIALIZED) {
        return;
    }

    CONNECTION_INITIALIZED = true;

    socket.on('harvester-connection', (appName, payload) => {
        store.commit(MutationsTypes.CONNECTION_SET_CONNECTION_DATA, {
            appName,
            ...payload
        });
        // offline
        if (store.state.connectionState.timeout) {
            clearTimeout(store.state.connectionState.timeout);
        }

        const connectionToSetOffline = setTimeout(() => {
            store.commit(MutationsTypes.CONNECTION_SET_OFFLINE, appName);
        }, TIMEOUT_DELAY);

        store.commit(MutationsTypes.CONNECTION_STATE_SET_VALUE, {
            timeout: connectionToSetOffline
        });

        // ping
        if (store.state.connectionState.ping.timeout) {
            clearInterval(store.state.connectionState.ping.timeout);
        }

        const pingInterval = setInterval(() => {
            // this.ping.value++

            store.commit(MutationsTypes.CONNECTION_STATE_SET_VALUE, {
                ping: {
                    value: store.state.connectionState.ping.value + 1,
                    timeout: pingInterval
                }
            });
        }, 1000);

        store.commit(MutationsTypes.CONNECTION_STATE_SET_VALUE, {
            ping: {
                value: 0,
                timeout: pingInterval
            }
        });
    });
};
