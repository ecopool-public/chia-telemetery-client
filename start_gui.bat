@echo off
CMD /C pm2 delete all
taskkill /IM chia.exe /F
taskkill /IM daemon.exe /F
taskkill /IM start_harvester.exe /F
taskkill /IM start_farmer.exe /F
taskkill /IM start_full_node.exe /F
taskkill /IM start_wallet.exe /F
taskkill /IM node.exe /F

CMD /C pm2 update

echo GUI IS STARTED
echo.
echo.
echo PRESS ENTER TO CLOSE WINDOW....
pause >nul