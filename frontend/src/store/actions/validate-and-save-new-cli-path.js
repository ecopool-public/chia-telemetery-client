import axios from "axios";
import * as ActionsTypes from './types';
import * as MutationsTypes from '../mutations/types';
import {ElNotification} from "element-plus";

export default async (store, {appName, cliPath}) => {
    store.commit(MutationsTypes.SET_LOADING, {
        text: 'Checking cli path',
        state: true
    });

    axios.post(`/api/user/check-path/${appName}`, {
        path: cliPath
    })
        .then((response) => {
            if (response.data.success) {
                return axios.post(`/api/user/set-path/${appName}`, {
                    path: cliPath
                })
                    .then((response) => {
                        if (response.data.success) {
                            store.commit(MutationsTypes.SET_LOADING, false);
                            ElNotification.success(`New cli path successfully setted`);
                            return store.dispatch(ActionsTypes.FETCH_CURRENCIES_DATA);
                        } else {
                            store.commit(MutationsTypes.SET_LOADING, false);
                            ElNotification.error(`Error set new path`);
                        }
                    });
            } else {
                store.commit(MutationsTypes.SET_LOADING, false);
                ElNotification.error(`Cannot find cli on this path`);
            }
        });
};