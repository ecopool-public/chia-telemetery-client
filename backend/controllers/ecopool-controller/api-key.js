const syncSocketRequest = require('./sync-socket-request');
const getLogs = require('../../utils/get-logs');
const { debugLog, debugError } = getLogs('ecopool-controller:api-key');

module.exports = async () => {
    let requestData;

    try {
        requestData = await syncSocketRequest('api-key');
    } catch (error) {
        debugError(JSON.stringify({
            message: 'Failed to get data from the server',
            error
        }));
        return { success: false };
    }

    debugLog('Api key received from the server');

    return {
        success: true,
        answer: requestData
    };
};
