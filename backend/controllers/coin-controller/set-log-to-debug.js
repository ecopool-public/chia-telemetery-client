const callCLIPromise = require('./call-cli-promise');
const getLogs = require('../../utils/get-logs');
const { debugLog, debugError } = getLogs('coin-controller:set-log-to-debug');

module.exports = async (appName) => {
    const checkCommand = await callCLIPromise(appName, {
        commandPayload: {
            configure: null,
            '--set-log-level': null,
            DEBUG: null
        },
        commandForStartHarvester: true
    });

    if (checkCommand.status) {
        debugLog(`${appName} logs installed successfully in DEBUG mode`);
    } else {
        debugError(`Failed to install ${appName} logs in DEBUG mode`);
    }

    return checkCommand.status;
};
