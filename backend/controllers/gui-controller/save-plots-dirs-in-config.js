const CoinController = require('../coin-controller');
const localConfig = require('./localConfig');
const getLogs = require('../../utils/getLogs');
const { debugLog, debugError } = getLogs('gui-controller:save-plots-dirs-in-config');

module.exports = async () => {
    const dirs = CoinController.getPlotsDirectories();
};
