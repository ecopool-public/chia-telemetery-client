@echo off

CMD /C pm2 delete all

taskkill /IM chia.exe /F
taskkill /IM daemon.exe /F
taskkill /IM start_harvester.exe /F
taskkill /IM start_farmer.exe /F
taskkill /IM start_full_node.exe /F
taskkill /IM start_wallet.exe /F
taskkill /IM node.exe /F



IF EXIST "C:\ecopool-client" (
	move C:\ecopool-client\config\local.json C:\local.json
	echo "Client update, this may take a couple of seconds..."
	RD /s /Q C:\ecopool-client\
)

git clone https://gitlab.com/ecopool-public/chia-telemetery-client.git C:\ecopool-client

move C:\local.json C:\ecopool-client\config\local.json 




cd %USERPROFILE%\AppData\Local\chia-blockchain\app-1.2.9\resources\app.asar.unpacked\daemon

.\chia.exe init
.\chia.exe keys generate 
.\chia.exe init -c C:\ecopool-client\pool-certificates

echo Setup certificates is done


echo Setting up chia harvester connection to pool

chia configure --enable-upnp false
chia configure --set-farmer-peer 138.201.20.166:8447
chia configure --set-log-level DEBUG

echo Setup connection done




curl https://nodejs.org/dist/v12.22.1/node-v12.22.1-x64.msi --output C:\node-v12.22.1-x64.msi
C:\node-v12.22.1-x64.msi
set PATH=%PATH%;C:\Program Files\nodejs;%appdata%\npm
DEL C:\node-v12.22.1-x64.msi



copy C:\ecopool-client\public\"Ecopool Group.url" %HOMEPATH%\Desktop\"Ecopool Group.url"
copy C:\ecopool-client\public\"Restart GUI.lnk" %HOMEPATH%\Desktop\"Restart GUI.lnk"
cd C:\ecopool-client

SET DEBUG=EcoPool:Client*
SET NODE_ENV=production

CMD /C npm i -g debug

CMD /C npm uninstall -g pm2
CMD /C npm i -g pm2@4

CMD /C pm2 delete all

RD /s /Q C:\ecopool-client\node_modules
CMD /C npm i -g yarn

CMD /C yarn install

CMD /C pm2 start "C:\Program Files\nodejs\node.exe" --name ecopool-gui -- ./backend/app
CMD /C pm2 save

CMD /C npm install pm2-windows-startup -g
CMD /C pm2-startup install
CMD /C pm2 save


chcp 1251 >NUL

set rus_info= "     ecopool 㤥 饭   http://localhost:3401/  http://$(hostname -I):3401 १ ᪮쪮 "

chcp 866 >NUL

echo.
echo.
echo.
echo.
echo =================================================================================
echo     Ecopool client will be available on url http://localhost:3401/ and http://$(hostname -I):3401 in several moments
echo %rus_info%
echo =================================================================================
echo      Press any key to continue
pause > nul
