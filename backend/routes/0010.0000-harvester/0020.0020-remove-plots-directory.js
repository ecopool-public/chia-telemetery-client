const router = require('express').Router();
const createError = require('http-errors');
const ROUTES_ERROR_ANSWERS = require('../../constants/routes-error-answers');
const GuiController = require('../../controllers/gui-controller');
const CoinController = require('../../controllers/coin-controller');

router.post('/remove-plots-directory/:appName', async (req, res, next) => {
    if (!GuiController.userIsLogged()) {
        return next(createError(401, ROUTES_ERROR_ANSWERS[401]));
    }

    const { body: { path } } = req;
    const {appName} = req.params;

    const removeStatus = await CoinController.removePlotsDirectory(appName, path);

    return res.send({ success: removeStatus });
});

module.exports = { router };
