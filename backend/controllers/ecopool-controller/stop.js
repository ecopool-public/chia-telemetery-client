const socketIoClient = require('./socket-io-client');
const socketIoClientForSendPlotsOnServer = require('./socket-io-client-for-send-plots-on-server');
const getLogs = require('../../utils/get-logs');
const { debugLog } = getLogs('ecopool-controller:stop');

module.exports = () => {
    socketIoClient.emit('dropUser');
    socketIoClientForSendPlotsOnServer.emit('dropUser');
    debugLog('The running ecopool controller processes have been successfully stopped');
};
