const router = require('express').Router();
const createError = require('http-errors');
const ROUTES_ERROR_ANSWERS = require('../../constants/routes-error-answers');
const GuiController = require('../../controllers/gui-controller');
const EcoPoolController = require('../../controllers/ecopool-controller');

router.get('/user/all-my-volumes', async (req, res, next) => {
    if (!GuiController.userIsLogged()) {
        return next(createError(401, ROUTES_ERROR_ANSWERS[401]));
    }
    let userVolumeData;

    try {
        userVolumeData = await EcoPoolController.userAllVolumes();
    } catch (error) {
        return res.send({ success: false });
    }

    if (!userVolumeData.success) {
        return res.send({ success: false });
    }

    return res.send({
        success: true,
        answer: userVolumeData.answer
    });
});

module.exports = { router };
