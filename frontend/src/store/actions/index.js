import * as ActionsTypes from './types';

import initialize from "./initialize";
import checkLogin from './check-login';
import fetchNewVersion from './fetch-new-version';
import fetchDashboardMetrics from './fetch-dashboard-metrics';
import fetchUserBalance from './fetch-user-balance';
import fetchUserPendingWithdrawals from './fetch-user-pending-withdrawals';
import fetchUserVolumes from './fetch-user-volumes';
import plotDirectoriesFetch from './plot-directories-fetch';
import plotDirectoriesAdd from './plot-directories-add';
import plotDirectoriesRemove from './plot-directories-remove';
import farmDataFetch from './farm-data-fetch';
import subscribeHarvesterConnection from './subscribe-harvester-connection';
import harvesterSubscribeProofsInfo from './harvester-subscribe-proofs-info';
import harvesterChartInfoSubscribe from './harvester-chart-info-subscribe';
import fetchHarvestersDetails from './fetch-harvesters-details';
import fetchCurrenciesData from './fetch-currencies-data';
import validateAndSaveNewCliPath from './validate-and-save-new-cli-path';
import restartHarvester from './restart-harvester';
import stopHarvester from './stop-harvester';
import subscribePointsInfo from './subscribe-points-info';
import setCurrentCurrency from './set-current-currency';

export default {
    [ActionsTypes.INITIALIZE]: initialize,
    [ActionsTypes.CHECK_LOGIN]: checkLogin,
    [ActionsTypes.FETCH_NEW_VERSION]: fetchNewVersion,
    [ActionsTypes.FETCH_DASHBOARD_METRICS]: fetchDashboardMetrics,
    [ActionsTypes.FETCH_USER_PENDING_WITHDRAWALS]: fetchUserPendingWithdrawals,
    [ActionsTypes.FETCH_USER_BALANCE]: fetchUserBalance,
    [ActionsTypes.FETCH_USER_VOLUMES]: fetchUserVolumes,
    [ActionsTypes.PLOT_DIRECTORIES_FETCH]: plotDirectoriesFetch,
    [ActionsTypes.PLOT_DIRECTORIES_ADD]: plotDirectoriesAdd,
    [ActionsTypes.PLOT_DIRECTORIES_REMOVE]: plotDirectoriesRemove,
    [ActionsTypes.FARM_DATA_FETCH]: farmDataFetch,
    [ActionsTypes.SUBSCRIBE_HARVESTER_CONNECTION]: subscribeHarvesterConnection,
    [ActionsTypes.HARVESTER_SUBSCRIBE_PROOFS_INFO]: harvesterSubscribeProofsInfo,
    [ActionsTypes.HARVESTER_CHART_INFO_SUBSCRIBE]: harvesterChartInfoSubscribe,
    [ActionsTypes.FETCH_HARVESTERS_DETAILS]: fetchHarvestersDetails,
    [ActionsTypes.FETCH_CURRENCIES_DATA]: fetchCurrenciesData,
    [ActionsTypes.VALIDATE_AND_SAVE_NEW_CLI_PATH]: validateAndSaveNewCliPath,
    [ActionsTypes.RESTART_HARVESTER]: restartHarvester,
    [ActionsTypes.STOP_HARVESTER]: stopHarvester,
    [ActionsTypes.SUBSCRIBE_POINTS_INFO]: subscribePointsInfo,
    [ActionsTypes.SET_CURRENT_CURRENCY]: setCurrentCurrency
}