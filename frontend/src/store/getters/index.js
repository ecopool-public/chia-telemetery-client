import * as GettersTypes from './types';

import getBalance from './get-balance';
import getId from './get-id';
import isAuth from './is-auth';
import isPathStatus from './is-path-status';

export default {
    [GettersTypes.GET_BALANCE]: getBalance,
    [GettersTypes.GET_ID]: getId,
    [GettersTypes.IS_AUTH]: isAuth,
    [GettersTypes.IS_PATH_STATUS]: isPathStatus
};