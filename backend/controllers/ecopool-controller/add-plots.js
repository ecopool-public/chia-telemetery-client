const socketIoClientForSendPlotsOnServer = require('./socket-io-client-for-send-plots-on-server');
const HarvesterInstance = require('../coin-controller/harvester-instance');
const setUser = require('./set-user');
const localConfig = require('../gui-controller/local-config');
const promiseTimeout = require('../../utils/promise-timeout');
const getLogs = require('../../utils/get-logs');
const { debugLog, debugError } = getLogs('ecopool-controller:add-plots');

const MAX_PLOTS_CHUNK_SIZE = 100;
const MAX_PLOTS_PING_CHUNK_SIZE = 200;
let plotsInDB = [];
let userForSocket = true;

const addPlotsInDb = (newPlots) => {
    if (!newPlots) {
        return;
    }

    const newPlotsKeys = Object.keys(newPlots);

    for (const plotKey of newPlotsKeys) {
        const hasPlot = plotsInDB.find(oldPlot => {
            return oldPlot.key === plotKey;
        });

        if (hasPlot) {
            continue;
        }

        plotsInDB.push({
            key: plotKey,
            plotId: newPlots[plotKey]
        });
    }
};
const filterPlots = (plots) => {
    if (!plotsInDB.length) {
        return plots;
    }

    return plots.filter(plot => {
        return !plotsInDB.find(plotInDB => {
            return plotInDB.key === `${plot.plotSeed}-${plot.memo}`;
        });
    });
};
const preparedPlotsForPing = (plots) => {
    return plots.map(plot => {
        const { plotId } = plot;
        return { plotId };
    });
};
const fetchPlots = async (plots) => {
    const filteredPlots = filterPlots(plots);
    const filteredPlotsLength = filteredPlots.length;
    const totalChunks = Math.ceil(filteredPlotsLength / MAX_PLOTS_CHUNK_SIZE);
    let currentChunkIdx = 0;

    try {
        for (let i = 0; i < filteredPlotsLength; i += MAX_PLOTS_CHUNK_SIZE) {
            const currentChunk = filteredPlots.slice(i, i+MAX_PLOTS_CHUNK_SIZE);
            currentChunkIdx++;

            debugLog(`Start sending data on plots to the server. Chunk ${currentChunkIdx} from ${totalChunks}`);

            await new Promise(async (resolve) => {
                let resolved = false;
                const resolvePromise = (payload) => {
                    if (payload) {
                        const { success, error, plotsData } = payload;

                        if (!success) {
                            debugError(JSON.stringify({
                                message: 'Error when sending a fetch chunk with plot data to the server',
                                error
                            }));

                            if ((error && error.userId === 'not found') && userForSocket) {
                                userForSocket = false;
                                setUser.plotsSocket(localConfig.get('currentUser'));
                            }
                        } else {
                            addPlotsInDb(plotsData);
                        }
                    }

                    if (!resolved) {
                        resolve();
                    }

                    resolved = true;
                };

                socketIoClientForSendPlotsOnServer.compress(true).emit('fetch-plots', currentChunk, resolvePromise);

                await promiseTimeout(5000);
                resolvePromise();
            });

            userForSocket = true;
            debugLog(`The end of sending data on plots to the server. Chunk ${currentChunkIdx} from ${totalChunks}`);
        }
    } catch (error) {
        debugError(JSON.stringify({
            message: 'Error sending fetch with plot data to the server',
            error
        }));
    }
};
const pingPlots = async (plots) => {
    const plotsPing = preparedPlotsForPing(plots);
    const plotsPingLength = plotsPing.length;
    const totalPlotsPingChunk = Math.ceil(plotsPingLength / MAX_PLOTS_PING_CHUNK_SIZE);
    let currentPingChunkIds = 0;

    try {
        for (let i = 0; i < plotsPingLength; i += MAX_PLOTS_PING_CHUNK_SIZE) {
            const currentChunk = plotsPing.slice(i, i+MAX_PLOTS_PING_CHUNK_SIZE);
            currentPingChunkIds++;

            debugLog(`Start sending ping on plots to the server. Chunk ${currentPingChunkIds} from ${totalPlotsPingChunk}`);

            await new Promise(async (resolve) => {
                let resolved = false;
                const resolvePromise = (payload) => {
                    if (payload) {
                        const { success, error } = payload;

                        if (!success) {
                            debugError(JSON.stringify({
                                message: 'Error when sending a chunk with pings on plots to the server',
                                error
                            }));
                        }

                        if ((error && error.userId === 'not found') && userForSocket) {
                            userForSocket = false;
                            setUser.plotsSocket(localConfig.get('currentUser'));
                        }
                    }

                    if (!resolved) {
                        resolve();
                    }

                    resolved = true;
                };

                socketIoClientForSendPlotsOnServer.compress(true).emit('ping-plots', currentChunk, resolvePromise);

                await promiseTimeout(2500);
                resolvePromise();
            });

            debugLog(`The end of sending ping on plots to the server. Chunk ${currentPingChunkIds} from ${totalPlotsPingChunk}`);
        }
    } catch (error) {
        debugLog(JSON.stringify({
            message: 'Error when sending pings by plots to the server',
            error
        }));
    }
};

module.exports = async (plots) => {
    HarvesterInstance.refreshPlots();
    await fetchPlots(plots);
    await pingPlots(plotsInDB);

    debugLog('Plot data has been successfully sent to the server');
    return true;
};
