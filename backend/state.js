/**
 * @typedef {'off' | 'check_cli' | 'stop_all' | 'set_certificates' | 'set_upnp' | 'set_farmer_peer' | 'set_log_to_debug' | 'start_harvester' | 'run'} HarvesterInitState
 */

/**
 * @typedef {Object} HarvesterStatus
 * @property {HarvesterInitState} stage
 * @property {'off' | 'in_progress' | 'running' | 'error'} runStatus
 */

/**
 * @readonly
 * @typedef {Object} AppState
 * @property {string|null} [token]
 * @property {Object.<string, HarvesterStatus>} harvesterIsWorking
 * @property {Object} queues
 * @property {Object} histories
 * @property {number} totalPlotsCount
 */

/**
 * @readonly
 * @type {AppState}
 */
const APP_STATE = {
    // init: false,
    harvesterIsWorking: {},
    token: null,
    queues: {},
    histories: {
        checkPlots: {},
        farmingPlots: {},
        proofs: {}
    },
    totalPlotsCount: 0
};

module.exports = APP_STATE;
