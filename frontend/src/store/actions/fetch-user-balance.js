import * as MutationsTypes from "../mutations/types";
import axios from "axios";

export default (store) => {
    store.commit(MutationsTypes.SET_DASHBOARD_WIDGET_LOADING, {
        balance: true
    });

    return axios.get(`/api/user/balance/${store.state.currencyId}?t=${Date.now()}`)
        .then(resp => {
            const respData = resp.data;
            if (!respData.success) {
                console.error('some error has occured', respData);
                return false;
            }
            const {answer: {amount}} = respData;
            store.commit(MutationsTypes.SET_BALANCE, amount);
        })
        .catch(error => {
            console.error(error);
            store.commit(MutationsTypes.SET_DASHBOARD_WIDGET_ERROR, {
                userBalance: true
            });
        })
        .finally(() => {
            store.commit(MutationsTypes.SET_DASHBOARD_WIDGET_LOADER, {
                userBalance: false
            });
            store.commit(MutationsTypes.SET_DASHBOARD_WIDGET_LOADING, {
                balance: false
            });
        });
};
