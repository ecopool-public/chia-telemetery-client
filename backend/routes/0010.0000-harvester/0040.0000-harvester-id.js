const router = require('express').Router();
const getLogs = require('../../utils/get-logs');
const { debugLog } = getLogs('routes:harvester:get-harvester-id');

const getHarvesterId = require('../../controllers/coin-controller/get-harvester-id');

router.get('/id/:appName', async (req, res, next) => {
    const {appName} = req.params;
    const harvesterId = getHarvesterId(appName);
    debugLog(harvesterId);

    return res.send({
        success: true,
        answer: harvesterId
    });
});

module.exports = { router };
