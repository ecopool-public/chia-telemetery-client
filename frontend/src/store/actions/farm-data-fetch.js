import * as MutationsTypes from '../mutations/types';
import axios from "axios";

import mixin from '../../mixins/iec-metrics';

export default async (store) => {
    store.commit(MutationsTypes.SET_LOADING, true);

    return axios.get(`/api/harvester/farm/${store.state.appName}`)
        .then((resp) => {
            if (resp.data.success) {
                const plots = resp.data.answer.plots.map((item, idx) => {
                    return {
                        index: idx + 1,
                        plotSize: mixin.methods.getIecValue(item.fileSize),
                        filename: item.filename,
                        path: item.path,
                        kSize: item.size,
                    }
                })
                const badPlots = resp.data.answer.badPlots.map(item => {
                    return {path: item}
                });

                store.commit(MutationsTypes.FARM_SET_PLOTS, plots);
                store.commit(MutationsTypes.FARM_SET_BAD_PLOTS, badPlots)
            }
        })
        .finally(() => {
            store.commit(MutationsTypes.SET_LOADING, false);
        });
};
