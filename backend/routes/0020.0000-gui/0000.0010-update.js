const router = require('express').Router();
const { exec } = require('child_process');
const promiseTimeout = require('../../utils/promise-timeout');
const getLogs = require('../../utils/get-logs');
const { debugLog, debugError } = getLogs('routes:gui:update');

router.get('/update', async (req, res) => {
    debugLog('Git pull process started');
    await new Promise(resolve => {
        exec('git pull', {
            cwd: process.cwd()
        }, (error, stdout, stderr) => {
            debugLog(stdout);

            if (stderr) {
                debugError(`Failed to execute git pull. ${stderr}`);
            }

            resolve();
        });
    });
    debugLog('The git pull process has ended');

    debugLog('Yarn install --prune process started');
    await new Promise(resolve => {
        exec('yarn install --prune', {
            cwd: process.cwd()
        }, (error, stdout, stderr) => {
            debugLog(stdout);

            if (stderr) {
                debugError(`Yarn install --prune failed. ${stderr}`);
            }

            resolve();
        });
    });
    debugLog('Yarn install --prune process finished');

    debugLog('A waiting process of 2 seconds has been started');
    await promiseTimeout(2000);
    debugLog('Wait 2 seconds completed');

    res.send();

    debugLog('Process pm2 restart all started');
    exec('pm2 restart all', {
        detached: true
    }, (error, stdout, stderr) => {
        debugLog(stdout);

        if (stderr) {
            debugError(`Failed to restart service. ${stderr}`);
        }
    });
    debugLog('Process pm2 restart all completed');
});

module.exports = { router };
