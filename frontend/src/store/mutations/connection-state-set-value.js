export default (state, payload) => {
    Object.keys(payload)
        .forEach((key) => {
            state.connectionState[key] = payload[key];
        });
};