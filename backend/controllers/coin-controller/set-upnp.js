const callCLIPromise = require('./call-cli-promise');
const getLogs = require('../../utils/get-logs');
const { debugLog, debugError } = getLogs('coin-controller:set-upnp');

module.exports = async (appName) => {
    const checkCommand = await callCLIPromise(appName, {
        commandPayload: {
            configure: null,
            '--enable-upnp': null,
            false: null
        },
        commandForStartHarvester: true
    });

    if (checkCommand.status) {
        debugLog(`Successfully set upnp = false for ${appName}`);
    } else {
        debugError(`Failed to set upnp = false for ${appName}`);
    }

    return checkCommand.status;
};
