const syncSocketRequest = require('./sync-socket-request');
const getLogs = require('../../utils/get-logs');
const { debugLog, debugError } = getLogs('ecopool-controller:user-balance');

module.exports = async (currencyId) => {
    let requestData;

    try {
        requestData = await syncSocketRequest('my-balance', currencyId);
    } catch (error) {
        debugError(JSON.stringify({
            message: 'Failed to get user balance from server',
            error
        }));
        return { success: false };
    }

    debugLog('User balance received from server');

    return {
        success: true,
        answer: requestData
    };
};
