import * as MutationsTypes from '../mutations/types';
import axios from 'axios';

export default async (store) => {
    store.commit(MutationsTypes.SET_LOADING, true);

    axios.get(`/api/gui/currencies?t=${Date.now()}`)
        .then(resp => {
            store.commit(MutationsTypes.SET_CURRENCIES_DATA, resp.data.answer);
        })
        .then(() => {
            store.commit(MutationsTypes.SET_LOADING, false);
        });
};