const router = require('express').Router();
const createError = require('http-errors');
const ROUTES_ERROR_ANSWERS = require('../../constants/routes-error-answers');
const GuiController = require('../../controllers/gui-controller');
const CoinController = require('../../controllers/coin-controller');

router.get('/farm/:appName', async (req, res, next) => {
    if (!GuiController.userIsLogged()) {
        return next(createError(401, ROUTES_ERROR_ANSWERS[401]));
    }

    const {appName} = req.params;

    const plots = await CoinController.getPlots(appName, true);
    const badPlots = await CoinController.getBadPlots(appName);

    return res.send({
        success: true,
        answer: { plots, badPlots }
    });
});

module.exports = { router };
