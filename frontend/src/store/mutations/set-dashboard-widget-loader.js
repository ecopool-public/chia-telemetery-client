export default (state, keyValue) => {
    Object.keys(keyValue)
        .forEach((key) => {
            state.dashboardWidgets.loaders[key] = keyValue[key];
        });
};