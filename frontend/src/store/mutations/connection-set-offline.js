export default (state, appName) => {
    if (appName !== state.appName) {
        return;
    }

    state.connectionState.quality = null;
    state.connectionState.status = null;
    state.connectionState.ping.value = 0;
    clearTimeout(state.connectionState.timeout);
    clearTimeout(state.connectionState.ping.timeout);

    state.connectionState.timeout = null;
    state.connectionState.ping.timeout = null;
};