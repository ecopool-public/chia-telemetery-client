const localConfig = require('./local-config');
const EcoPoolController = require('../ecopool-controller');
const CoinController = require('../coin-controller');
const checkConnectionSettings = require('../../utils/check-connection-settings');
const compareConnections = require('../../utils/compare-connections');
const getLogs = require('../../utils/get-logs');
const { debugLog, debugWarn, debugError } = getLogs('gui-controller:get-connection');
const state = require('../../state');

/**
 * @param appName
 * @param onlyGetConnectionData
 * @returns {Promise<{port, ip}|boolean>}
 */
module.exports = async (appName, onlyGetConnectionData = false) => {
    const user = localConfig.get('currentUser');
    const userId = localConfig.get('userId');

    if (!user || !userId) {
        debugWarn('Haven`t user or user id or both');
        return false;
    }

    const connectionData = await EcoPoolController.currencies()
        .then((currencies) => {
            return currencies.find(item => item.appName === appName)
                .connectionInfo;
        });

    if (onlyGetConnectionData) {
        return connectionData;
    }

    const setFarmerPeerStatus = await CoinController.setFarmerPeer(appName, connectionData);

    if (!setFarmerPeerStatus) {
        return false;
    }

    debugLog(`Connection data successfully setup for ${appName}`);

    CoinController.restartHarvester(appName);
};
