@echo off
cd C:\ecopool-client

SET DEBUG=EcoPool:Client*
SET NODE_ENV=production

CMD /C npm i -g debug

CMD /C npm uninstall -g pm2
CMD /C npm i -g pm2@4

CMD /C pm2 delete all

CMD /C pm2 start "C:\Program Files\nodejs\node.exe" --name ecopool-gui -- ./backend/app
CMD /C pm2 save

CMD /C npm install pm2-windows-startup -g
CMD /C pm2-startup install
CMD /C pm2 save



chcp 1251 >NUL

set rus_info= "    ������ ecopool ����� ������� �� ������ http://localhost:3401/ � http://$(hostname -I):3401 ����� ��������� ���������"

chcp 866 >NUL

echo.
echo.
echo.
echo.
echo =================================================================================
echo     Ecopool client will be available on url http://localhost:3401/ and http://$(hostname -I):3401 in several moments
echo %rus_info%
echo =================================================================================
echo      Press any key to continue
pause > nul