import { createRouter, createWebHistory } from 'vue-router'
import { store } from '../store';
import * as MutationsTypes from '../store/mutations/types';
import * as GettersTypes from '../store/getters/types';

// unauth
import RegistrationPage from '/src/pages/Registration';
import LoginPage from '/src/pages/Login';

// auth
import DashboardPage from '/src/pages/Dashboard';
import WalletPage from '/src/pages/Wallet';
import FarmPage from '/src/pages/Farm';
import RequestChiaPathPage from '/src/pages/RequestChiaPathPage';
import Plotting from '/src/pages/Plotting';
import PlotsDirectories from "../pages/PlotsDirectories";
import PlotsCreate from "../pages/PlotsCreate";
import Mnemonics from '../pages/Mnemonics';
import CurrenciesSetup from "../pages/CurrenciesSetup";

function isAuth (to, from, next) {
    switch(true) {
        case store.getters[GettersTypes.IS_AUTH]:
            return next();
        default:
            return next('/login');
    }
}

function isSetPath (to, from, next) {
    if (store.getters.isAuth && !store.getters.isPathStatus) {
        return next()
    }
    store.commit(MutationsTypes.LOGOUT);
    return next('/login');
}

function isUnAuth (to, from, next) {
    if (!store.state.isAuth) {
        return next()
    }
    return next('/currencies-setup');
}

const routes = [
    {
        path: '/registration',
        name: 'Registration',
        component: RegistrationPage,
        beforeEnter: isUnAuth
    },
    {
        path: '/login',
        name: 'Login',
        component: LoginPage,
        beforeEnter: isUnAuth
    },
    {
        path: '/dashboard',
        name: 'Dashboard',
        component: DashboardPage,
        beforeEnter: isAuth
    },
    {
        path: '/wallet',
        name: 'Wallet',
        component: WalletPage,
        beforeEnter: isAuth
    },
    {
        path: '/plotting',
        name: 'Plotting',
        component: Plotting,
        beforeEnter: isAuth
    },
    {
        path: '/plots-directories',
        name: 'PlotsDirectories',
        component: PlotsDirectories,
        beforeEnter: isAuth
    },
    {
        path: '/plots/create',
        name: 'PlotsCreate',
        component: PlotsCreate,
        beforeEnter: isAuth
    },
    {
        path: '/farm',
        name: 'Farm',
        component: FarmPage,
        beforeEnter: isAuth,
    },
    {
        path: '/mnemonics',
        name: 'Mnemonics',
        component: Mnemonics,
        beforeEnter: isAuth
    },
    {
        path: '/currencies-setup',
        name: 'CurrenciesSetup',
        component: CurrenciesSetup,
        beforeEnter: isAuth
    },
    {
        path: '/request_chia_path',
        name: 'RequestChiaPath',
        component: RequestChiaPathPage,
        beforeEnter: isSetPath,
    },
    {
        path: '/:pathMatch(.*)*',
        redirect: '/login',
    },
]

// noinspection JSCheckFunctionSignatures
const router = createRouter({
    history: createWebHistory(),
    routes
})

export default router
