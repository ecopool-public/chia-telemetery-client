const ECO_POOL_API = require('../../constants/eco-pool-api');
const apiRequest = require('./api-request');
const setCookie = require('set-cookie-parser');
const setUser = require('./set-user');
const getLogs = require('../../utils/get-logs');
const { debugLog, debugWarn, debugError } = getLogs('ecopool-controller:registration');
const clearErrorData = require('../../utils/clear-error-data');
const state = require('../../state');

module.exports = async (payload) => {
    const { login } = payload;
    let requestData;

    try {
        requestData = await apiRequest({
            url: ECO_POOL_API.registration,
            data: payload
        });
    } catch (error) {
        debugError(JSON.stringify({
            message: `When registering a user with email: ${login}, an error occurred on the server`,
            error: clearErrorData(error)
        }));
        return { success: false };
    }

    const {
        status,
        data: { success }
    } = requestData;
    const parseCookies = setCookie.parse(requestData.headers['set-cookie']);
    const cookieSid = parseCookies.find(cookie => {
        return cookie.name === 'connect.sid';
    });
    const token = cookieSid && cookieSid.value || null;

    if (status !== 200) {
        debugError(`Server response code: ${status}`);
        return { success: false };
    }

    if (!success) {
        debugWarn(JSON.stringify({
            message: `User registration denied with email: ${login}`,
            reason: requestData.data.answer
        }));
        return {
            success: false,
            answer: requestData.data.answer
        };
    }

    debugLog(`User registration with email: ${login}, was successful`);
    setUser.allSockets(login);

    if (token) {
        state.token = token;
    }

    return { success: true };
};
