export default (state, payload) => {
    const {appName, filter} = payload;

    if (!state.harvesterProofsTable[appName]) {
        state.harvesterProofsTable[appName] = [];
    }

    if (state.harvesterProofsTable[appName].length > 10) {
        state.harvesterProofsTable[appName].pop();
    }
    state.harvesterProofsTable[appName].unshift(filter);
};