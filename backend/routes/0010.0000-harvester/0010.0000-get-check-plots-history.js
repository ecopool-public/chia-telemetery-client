const router = require('express').Router();
const createError = require('http-errors');
const ROUTES_ERROR_ANSWERS = require('../../constants/routes-error-answers');
const GuiController = require('../../controllers/gui-controller');
const getLogs = require('../../utils/get-logs');
const { debugLog } = getLogs('routes:harvester:get-check-plots-history');
const state = require('../../state');

router.get('/get-check-plots-history', async (req, res, next) => {
    if (!GuiController.userIsLogged()) {
        return next(createError(401, ROUTES_ERROR_ANSWERS[401]));
    }

    debugLog('The plots check history has been successfully sent to the client');

    return res.send({
        success: true,
        answer: {
            history: state.histories.checkPlots
        }
    });
});

module.exports = { router };
