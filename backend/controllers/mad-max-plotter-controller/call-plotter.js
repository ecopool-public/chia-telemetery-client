const path = require('path');
const { spawn, exec } = require('child_process');
const config = require('config');
const getPreparedPayloadForCommand = require('../../utils/get-prepared-payload-for-command');
const getLogs = require('../../utils/get-logs');

module.exports = (payload = {}) => {
    const plotterPath = process.platform === 'win32'
        ? path.join(process.cwd(), '/plotters/chia_plot.exe')
        : path.join(process.cwd(), '/plotters/chia_plot')
    ;
    const defaultCommandOptions = {
        shell: true,
        detached: true
    };
    const payloadForCommand = getPreparedPayloadForCommand(payload);
    const command = `${plotterPath} ${payloadForCommand}`;
    let child;

    if (process.platform === 'win32') {
        child = exec(command, {
            ...defaultCommandOptions,
            windowsHide: true
        })
    } else {
        child = spawn(command, defaultCommandOptions);
    }

    const pid = child.pid;
    const logs = getLogs(`chia-controller:call-cli:${pid}`);
    const pidLog = logs.debugLog;
    const pidError = logs.debugError;
    const isDebugSpawnCommand = config.debug.spawnCommand;

    child.stderr.on('data', (data) => {
        const string = data.toString().trim();

        if (isDebugSpawnCommand) {
            pidError(string);
        }
    });
    child.stdout.on('data', (data) => {
        const string = data.toString().trim();

        if (isDebugSpawnCommand) {
            pidLog(string);
        }
    });
    child.on('exit', (exitCode) => {
        if (exitCode === 0) {
            pidLog(`Child exited with code: ${exitCode}. SUCCESS`);
        } else {
            pidError(`Child exited with code: ${exitCode}. ERROR`);
        }
    });

    return child;
};
