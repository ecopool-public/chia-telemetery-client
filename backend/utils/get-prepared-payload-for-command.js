module.exports = (payload) => {
    return Object.keys(payload)
        .reduce((acc, key) => {
            acc.push(key);

            if (payload[key]) {
                acc.push(payload[key]);
            }

            return acc;
        }, []).join(' ');
};
