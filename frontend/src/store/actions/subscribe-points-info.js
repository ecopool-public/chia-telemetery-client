import socket from "../../sockets";
import * as MutationsTypes from '../mutations/types';

let CONNECTION_INITIALIZED = false;

export default async (store) => {
    if (CONNECTION_INITIALIZED) {
        return;
    }

    CONNECTION_INITIALIZED = true;

    socket.on('points-info', (payload) => {
        const currencyId = store.state.currencyId;

        if (typeof payload === 'string') {
            payload = JSON.parse(payload);
        }

        const pool = ((payload.pool[currencyId] || {}).estimated_volume || 0);
        const user = ((payload.user[currencyId] || {}).estimated_volume || 0);

        store.commit(MutationsTypes.SET_DASHBOARD_WIDGET_VALUES, {
            poolNetSpace: Number(pool),
            userNetSpace: Number(user),
            userPlots: store.state.dashboardWidgets.userPlots
        });
    });
};
