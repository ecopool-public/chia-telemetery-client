import * as MutationsTypes from '../mutations/types';
import axios from "axios";
import socket from "../../sockets";

let INITIALIZED = false;

export default async (store) => {
    if (INITIALIZED) {
        return;
    }

    INITIALIZED = true;

    let canAppend = false;

    axios.get('/api/harvester/get-telemetry-history')
        .then((resp) => {
            if (resp.data.success) {
                Object.keys(resp.data.answer.history)
                    .forEach((appName) => {
                        store.commit(MutationsTypes.HARVESTER_CHART_INFO_SET, {
                            appName,
                            payload: resp.data.answer.history[appName]
                        });
                    });
            }
        })
        .finally(() => {
            canAppend = true;
        });

    socket.on('harvester-telemetry', (appName, payload) => {
        if (!canAppend) {
            return;
        }

        store.commit(MutationsTypes.HARVESTER_CHART_INFO_APPEND, {
            appName,
            ...payload
        });
    });
};
