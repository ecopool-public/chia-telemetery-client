const router = require('express').Router();
const createError = require('http-errors');
const ROUTES_ERROR_ANSWERS = require('../../constants/routes-error-answers');
const GuiController = require('../../controllers/gui-controller');
const getCreatingPlotByIdAndQueueName = require('../../utils/get-creating-plot-by-id-and-queue-name');

router.post('/get-creating-plot-logs', async (req, res, next) => {
    if (!GuiController.userIsLogged()) {
        return next(createError(401, ROUTES_ERROR_ANSWERS[401]));
    }

    const { body: { queueName, plotId } } = req;
    const currentPlotData = getCreatingPlotByIdAndQueueName(queueName, plotId);

    return res.send({
        success: true,
        answer: { logs: currentPlotData.logs }
    });
});

module.exports = { router };
