#!/bin/bash

#stop client
pm2 stop all

#stop chia
cd ~/chia-blockchain/
. ./activate
chia stop all -d
chia stop all
deactivate

#reinstall chia
cd ~
rm -rf chia-blockchain/
git clone https://github.com/Chia-Network/chia-blockchain.git -b latest --recurse-submodules
cd chia-blockchain/
sh install.sh

#restart client
pm2 restart all
echo "wait 30 sec."
sleep 30
cd ~/chia-blockchain/
. ./activate
chia init
chia start harvester -r
deactivate

#chmod exec
cd ~/ecopool-client/
chmod +x update-chia.sh

####pm2 log 0
echo "update completed!"
