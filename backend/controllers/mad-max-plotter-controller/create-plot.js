const config = require('config');
const DEFAULT_PLOTS_RAM_SIZES = require('../../constants/default-plots-ram-sizes');
const callPlotter = require('./call-plotter');
const getLogs = require('../../utils/get-logs');
const { debugLog, debugWarn, debugError } = getLogs('mad-max-plotter-controller:create-plot');
const poolKeys = config.poolKeys;

module.exports = (payload) => {
    let {
        ram,
        threads,
        buckets,
        tempFolder,
        tempFolder2,
        persistentFolder
    } = payload;

    if (!tempFolder) {
        debugError('Temporary folder not transferred');
        return false;
    }

    if (!persistentFolder) {
        debugError('Persistent folder not transferred');
        return false;
    }

    if (!ram) {
        debugWarn(`The size of the RAM was not transferred, ${DEFAULT_PLOTS_RAM_SIZES[32]} MiB is set by default`);
        ram = DEFAULT_PLOTS_RAM_SIZES[32];
    }

    if (!threads) {
        debugWarn('The number of threads is not transmitted, the default is 2');
        threads = 2;
    }

    if (!buckets) {
        debugWarn('The number of baskets has not been transmitted, the default is 256');
        buckets = 256;
    }

    const localPoolKeys = poolKeys[(payload.size && payload.size < 32) ? 'xcc' : 'xch'];

    const plotParams = {
        // '-b': ram,
        '-r': threads,
        '-u': buckets,
        '-n': 1,
        '-t': tempFolder,
        '-d': persistentFolder,
        '-f': localPoolKeys.farmer,
        '-p': localPoolKeys.pool
    };

    if (payload.size && payload.size < 32) {
        plotParams['-k'] = payload.size;
        plotParams['-x'] = 9699;
        // delete plotParams['-b'];
    }

    if (tempFolder2) {
        plotParams['-2'] = tempFolder2;
    }

    const stringPlotParams = `size: ${payload.size}, threads: ${threads}, buckets: ${buckets}, tempFolder: ${tempFolder}, ${tempFolder2 ? `tempFolder2: ${tempFolder2}, ` : ''} persistentFolder: ${persistentFolder}`;

    debugLog(`Creation plot with parameters: (${stringPlotParams}) started`);

    return callPlotter(plotParams);
};
