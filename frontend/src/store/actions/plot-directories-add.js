import * as MutationsTypes from '../mutations/types';
import * as ActionsTypes from './types';
import axios from "axios";
import ElNotification from "element-plus/lib/el-notification";

export default async (store, pathToAdd) => {
    store.commit(MutationsTypes.SET_LOADING, true);

    try {
        let resp = await axios.post(`/api/harvester/add-plots-directory/${store.state.appName}`, {
            path: pathToAdd
        });
        if (resp.data.success) {
            ElNotification.success({
                title: 'Success',
                message: 'Directory was added',
            });
            await store.dispatch(ActionsTypes.PLOT_DIRECTORIES_FETCH);

            return true;
        } else {
            ElNotification.error({
                title: 'Error',
                message: "Couldn't add a plot directory",
            });
            return false;
        }
    } catch(error) {
        console.error(error);
        return false;
    }
};
