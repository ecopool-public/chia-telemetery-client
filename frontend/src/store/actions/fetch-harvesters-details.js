import * as MutationsTypes from '../mutations/types';
import axios from 'axios';
import mixin from '../../mixins/iec-metrics';

export default async (store) => {
    store.commit(MutationsTypes.SET_LOADING, true);

    return Promise.all([
        axios.get(`/api/user/all-my-volumes?t=${Date.now()}`)
            .then(resp => {
                const sizes = (resp.data.answer || [])
                    .map((item) => {
                        return {
                            ...item,
                            volume: mixin.methods.getIecValue(item.estimated_volume)
                        }
                    });

                store.commit(MutationsTypes.SET_HARVESTERS_DETAILS, sizes);
            }),
        axios.get(`/api/harvester/id/${store.state.appName}?t=${Date.now()}`)
            .then(resp => {
                store.commit(MutationsTypes.SET_HARVESTER_ID, resp.data.answer);
            })
    ])
        .then(() => {
            store.commit(MutationsTypes.SET_LOADING, false);
        });
};