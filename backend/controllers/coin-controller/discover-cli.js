const { readdirSync } = require('fs');
const semver = require('semver');
const {homedir} = require('os');

const localConfig = require('../gui-controller/local-config');

/**
 * @param {string} appName
 * @returns {string|undefined|T}
 */
module.exports = (appName) => {
    if (localConfig.get(`clientsSettings.${appName}.cliPath`)) {
        return localConfig.get(`clientsSettings.${appName}.cliPath`);
    }

    if (process.platform === 'win32') {
        const sourcePath = `${homedir()}\\AppData\\Local\\${appName}-blockchain\\`;

        let semverDirs = [];
        try {
            // noinspection JSCheckFunctionSignatures
            semverDirs = readdirSync(sourcePath, {
                withFileTypes: true,
                encoding: 'utf8'
            })
                .filter(directory => {
                    return directory.isDirectory() && semver.valid(directory.name.replace('app-', ''));
                })
                .map(directory => {
                    return directory.name.replace('app-', '');
                });
        } catch(e) {

        }

        if (!semverDirs.length) {
            return undefined;
        } else {
            const bestVersion = semverDirs.sort(semver.compare).reverse()[0];
            return `"${homedir()}\\AppData\\Local\\${appName}-blockchain\\app-${bestVersion}\\resources\\app.asar.unpacked\\daemon\\${appName}.exe"`;
        }
    } else {
        return `~/${appName}-blockchain`;
    }
};
