import * as MutationsTypes from '../mutations/types';
import * as ActionsTypes from './types';
import axios from "axios";
import {ElNotification} from "element-plus";

export default async (store, appName) => {
    store.commit(MutationsTypes.SET_LOADING, {
        state: true,
        text: `Restarting harvester for ${appName}`
    });

    axios.get(`/api/harvester/restart/${appName}`)
        .then((response) => {
            if (response.data.success) {
                ElNotification.success('Harvester successfully restarted');
            } else {
                ElNotification.error(`Error starting harvester`);
            }
        })
        .finally(() => {
            store.commit(MutationsTypes.SET_LOADING, false);
            return store.dispatch(ActionsTypes.FETCH_CURRENCIES_DATA);
        });
}