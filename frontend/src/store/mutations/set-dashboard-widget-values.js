export default (state, keyValue) => {
    Object.keys(keyValue)
        .forEach((key) => {
            state.dashboardWidgets[key] = keyValue[key];
        });
};