const router = require('express').Router();
const createError = require('http-errors');
const ROUTES_ERROR_ANSWERS = require('../../constants/routes-error-answers');
const GuiController = require('../../controllers/gui-controller');
const CoinController = require('../../controllers/coin-controller');

router.post('/add-plots-directory/:appName', async (req, res, next) => {
    const {appName} = req.params;
    if (!GuiController.userIsLogged()) {
        return next(createError(401, ROUTES_ERROR_ANSWERS[401]));
    }

    const { body: { path } } = req;
    const addStatus = await CoinController.addPlotsDirectory(appName, path);

    return res.send({ success: addStatus });
});

module.exports = { router };
