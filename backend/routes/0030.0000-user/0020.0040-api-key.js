const router = require('express').Router();
const createError = require('http-errors');
const ROUTES_ERROR_ANSWERS = require('../../constants/routes-error-answers');
const GuiController = require('../../controllers/gui-controller');
const EcoPoolController = require('../../controllers/ecopool-controller');

router.get('/user/api-key', async (req, res, next) => {
    if (!GuiController.userIsLogged()) {
        return next(createError(401, ROUTES_ERROR_ANSWERS[401]));
    }

    let apiKeyData;
    try {
        apiKeyData = await EcoPoolController.apiKey();
    } catch (error) {
        return res.send({ success: false });
    }

    return res.send({
        success: true,
        answer: {
            apiKey: apiKeyData.answer
        }
    });
});

module.exports = { router };
