const syncSocketRequest = require('./sync-socket-request');
const getLogs = require('../../utils/get-logs');
const { debugLog, debugError } = getLogs('ecopool-controller:get-mnemonics');

module.exports = async () => {
    let requestData;

    try {
        requestData = await syncSocketRequest('list-mnemonic');
    } catch (error) {
        debugError(JSON.stringify({
            message: 'Failed to get mnemonics from server',
            error
        }));
        return { success: false };
    }

    debugLog('User mnemonics retrieved from server');

    return requestData;
}
