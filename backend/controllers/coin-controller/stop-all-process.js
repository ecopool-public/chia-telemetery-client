const callCLIPromise = require('./call-cli-promise');
const getLogs = require('../../utils/get-logs');
const { debugLog, debugWarn } = getLogs('coin-controller:stop-all-process');
const state = require('../../state');

module.exports = async (appName) => {
    const checkCommand = await callCLIPromise(appName, {
        commandPayload: {
            stop: null,
            all: null,
            '-d': null
        },
        commandForStartHarvester: true
    });

    if (checkCommand.status) {
        state.harvesterIsWorking = false;
        debugLog(`All ${appName} processes stopped successfully`);
    } else {
        debugWarn(`Failed to stop all ${appName} processes`);
    }

    return checkCommand.status;
};
