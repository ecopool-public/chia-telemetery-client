import * as MutationsTypes from "../mutations/types";
import axios from "axios";

export default async (store) => {
    store.commit(MutationsTypes.SET_DASHBOARD_WIDGET_LOADING, {
        pendingWithdrawal: true
    });

    return axios.get(`/api/user/pending-withdrawals/${store.state.currencyId}?t=${Date.now()}`)
        .then(resp => {
            const respData = resp.data;

            if (!respData.success) {
                console.error('some error has occured', respData);
                return false;
            }

            const {answer} = respData;

            store.commit(MutationsTypes.SET_PENDING_WITHDRAWALS, answer);
        })
        .catch(error => {
            console.error(error);
            store.commit(MutationsTypes.SET_DASHBOARD_WIDGET_ERROR, {
                pendingWithdrawal: true
            });
        })
        .finally(() => {
            store.commit(MutationsTypes.SET_DASHBOARD_WIDGET_LOADER, {
                pendingWithdrawal: false
            });
            store.commit(MutationsTypes.SET_DASHBOARD_WIDGET_LOADING, {
                pendingWithdrawal: false
            });
        });
};
