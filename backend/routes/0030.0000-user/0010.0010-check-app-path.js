const router = require('express').Router();
const createError = require('http-errors');
const ROUTES_ERROR_ANSWERS = require('../../constants/routes-error-answers');
const GuiController = require('../../controllers/gui-controller');
const CoinController = require('../../controllers/coin-controller');

router.post('/user/check-path/:appName', async (req, res, next) => {
    const {appName} = req.params;
    const { body: { path } } = req;

    if (!GuiController.userIsLogged()) {
        return next(createError(401, ROUTES_ERROR_ANSWERS[401]));
    }

    if (!path) {
        return { success: false };
    }

    const checkCLI = await CoinController.checkCLI(appName, path);

    if (checkCLI) {
        return res.send({ success: true });
    } else {
        return res.send({ success: false });
    }
});

module.exports = { router };
