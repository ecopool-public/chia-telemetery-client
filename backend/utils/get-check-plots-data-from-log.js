const stringRegex = /Loaded a total of [\d]{1,} plots of size [\d.]{1,} TiB, in [\d.]{1,} seconds/gm;
const countPlotsDataRegex = /total of [\d]{1,} plots/gm;
const countPlotsSizeDataRegex = /size [\d.]{1,} TiB/gm;
const timeDataRegex = /[\d.]{1,} seconds/gm;
const numberRegex = /[\d.]{1,}/gm;

const stringRegex125 = /total_result.loaded_plots\s(\d+).*total_result.loaded_size\s([\d\.]+).*total_duration\s([\d\.]+)/gm;

module.exports = (logString) => {
    const match123 = logString.match(stringRegex);
    const match125 = logString.match(stringRegex125);

    if (
        logString.indexOf('chia.plotting.plot_tools') !== -1 && match123
    ) {
        const stringWithData = match123[0];
        const stringWithCountPlotsData = stringWithData.match(countPlotsDataRegex)[0];
        const stringWithPlotsSizeData = stringWithData.match(countPlotsSizeDataRegex)[0];
        const stringWithTimeData = stringWithData.match(timeDataRegex)[0];

        return {
            count: stringWithCountPlotsData.match(numberRegex)[0],
            size: stringWithPlotsSizeData.match(numberRegex)[0],
            time: stringWithTimeData.match(numberRegex)[0]
        };
    } else if (match125) {
        const [_, count, size, time] = stringRegex125.exec(logString);

        return {
            count, size, time
        };
    } else {
        return false;
    }


};
