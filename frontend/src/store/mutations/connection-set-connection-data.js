export default (state, payload) => {
    const {appName} = payload;

    if (appName !== state.appName) {
        return;
    }

    const {connection} = payload;

    state.connectionState.quality = connection;
    state.connectionState.loading = false;
    state.connectionState.status = true;
};