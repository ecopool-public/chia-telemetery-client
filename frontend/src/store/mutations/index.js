import * as MutationsTypes from './types';

import logout from "./logout";
import setAuth from './set-auth';
import setBalance from './set-balance';
import setBlockVersion from './set-block-version';
import setId from './set-id';
import setNewVersion from './set-new-version';
import setPathStatus from './set-path-status';
import setPendingWithdrawals from './set-pending-withdrawals';
import setLoading from './set-loading';

import setDashboardWidgetError from './set-dashboard-widget-error';
import setDashboardWidgetLoader from './set-dashboard-widget-loader';
import setDashboardWidgetLoading from './set-dashboard-widget-loading';
import setDashboardWidgetValues from './set-dashboard-widget-values';

import plotDirectoriesSet from './plot-directories-set';

import farmSetPlots from './farm-set-plots';
import farmSetBadPlots from './farm-set-bad-plots';

import connectionSetConnectionData from './connection-set-connection-data';
import connectionSetOffline from './connection-set-offline';
import connectionStateSetValue from './connection-state-set-value';

import harvesterProofsInfoAppend from './harvester-proofs-info-append';

import harvesterChartInfoAppend from './harvester-chart-info-append';
import harvesterChartInfoSet from './harvester-chart-info-set';

import setHarvesterId from './set-harvester-id';
import setHarvestersDetails from './set-harvesters-details';

import setCurrenciesData from './set-currencies-data';
import setCurrentCurrency from './set-current-currency';

import setCurrencyPrice from "./set-currency-price";

export default {
    [MutationsTypes.LOGOUT]: logout,
    [MutationsTypes.SET_AUTH]: setAuth,
    [MutationsTypes.SET_BALANCE]: setBalance,
    [MutationsTypes.SET_BLOCK_VERSION]: setBlockVersion,
    [MutationsTypes.SET_ID]: setId,
    [MutationsTypes.SET_NEW_VERSION]: setNewVersion,
    [MutationsTypes.SET_PATH_STATUS]: setPathStatus,
    [MutationsTypes.SET_PENDING_WITHDRAWALS]: setPendingWithdrawals,
    [MutationsTypes.SET_LOADING]: setLoading,
    [MutationsTypes.SET_DASHBOARD_WIDGET_ERROR]: setDashboardWidgetError,
    [MutationsTypes.SET_DASHBOARD_WIDGET_LOADER]: setDashboardWidgetLoader,
    [MutationsTypes.SET_DASHBOARD_WIDGET_LOADING]: setDashboardWidgetLoading,
    [MutationsTypes.SET_DASHBOARD_WIDGET_VALUES]: setDashboardWidgetValues,
    [MutationsTypes.PLOT_DIRECTORIES_SET]: plotDirectoriesSet,
    [MutationsTypes.FARM_SET_PLOTS]: farmSetPlots,
    [MutationsTypes.FARM_SET_BAD_PLOTS]: farmSetBadPlots,
    [MutationsTypes.CONNECTION_SET_OFFLINE]: connectionSetOffline,
    [MutationsTypes.CONNECTION_SET_CONNECTION_DATA]: connectionSetConnectionData,
    [MutationsTypes.CONNECTION_STATE_SET_VALUE]: connectionStateSetValue,
    [MutationsTypes.HARVESTER_PROOFS_INFO_APPEND]: harvesterProofsInfoAppend,
    [MutationsTypes.HARVESTER_CHART_INFO_APPEND]: harvesterChartInfoAppend,
    [MutationsTypes.HARVESTER_CHART_INFO_SET]: harvesterChartInfoSet,
    [MutationsTypes.SET_HARVESTER_ID]: setHarvesterId,
    [MutationsTypes.SET_HARVESTERS_DETAILS]: setHarvestersDetails,
    [MutationsTypes.SET_CURRENCIES_DATA]: setCurrenciesData,
    [MutationsTypes.SET_CURRENT_CURRENCY]: setCurrentCurrency,
    [MutationsTypes.SET_CURRENCY_PRICE]: setCurrencyPrice
}