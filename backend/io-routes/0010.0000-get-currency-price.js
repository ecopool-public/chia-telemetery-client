const EcoPoolController = require('../controllers/ecopool-controller');
const getLogs = require('../utils/get-logs');
const { debugLog } = getLogs('io-routes:new-mnemonics');

const getCurrencyPrice = require('../../common/currencies-prices');

module.exports = {
    init (socket) {
        socket.on('get-currency-price', (currency, cb) => {
            cb(getCurrencyPrice(currency));
        });
    }
};
