const router = require('express').Router();
const GuiController = require('../../controllers/gui-controller');
const CoinController = require('../../controllers/coin-controller');

router.get('/check-login', async (req, res) => {
    const checkClients = await CoinController.checkAllClients();

    const answer = {
        success: GuiController.userIsLogged(),
        answer: {
            checkClients
        }
    };

    return res.send(answer);
});

module.exports = { router };
