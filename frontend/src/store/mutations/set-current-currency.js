export default (store, currencyItem) => {
    store.appName = currencyItem.appName;
    store.currencyId = currencyItem.id;
    store.currencyPrice = 0;

    localStorage.setItem('appName', currencyItem.appName);
    localStorage.setItem('currencyId', currencyItem.id);
}