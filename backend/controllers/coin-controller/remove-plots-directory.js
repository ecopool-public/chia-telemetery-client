const getHarvesterInstance = require('./harvester-instance');
const getLogs = require('../../utils/get-logs');
const { debugLog, debugWarn, debugError } = getLogs('coin-controller:remove-plots-directory');
const state = require('../../state');

/**
 * @param {string} appName
 * @param {string} directory
 * @returns {Promise<boolean|*>}
 */
module.exports = async (appName, directory) => {
    if (!directory) {
        debugError('Directory with plots not specified');
        return false;
    }

    if (!state.harvesterIsWorking) {
        debugWarn('Harvester not started. Command could not be executed');
        return false;
    }

    const HarvesterInstance = await getHarvesterInstance(appName);

    const { success } = await HarvesterInstance.removePlotDirectory(directory);

    if (success) {
        debugLog(`Directory ${directory} with plots deleted successfully for ${appName}`);
    } else {
        debugError(`Failed to delete directory ${directory} with plots for ${appName}`);
    }

    return success;
};
