const router = require('express').Router();
const createError = require('http-errors');
const ROUTES_ERROR_ANSWERS = require('../../constants/routes-error-answers');
const GuiController = require('../../controllers/gui-controller');
const CoinController = require('../../controllers/coin-controller');
const state = require('../../state');

router.get('/get-plots-directories/:appName', async (req, res, next) => {
    const {appName} = req.params;

    if (!GuiController.userIsLogged()) {
        return next(createError(401, ROUTES_ERROR_ANSWERS[401]));
    }

    if (!state.harvesterIsWorking) {
        return res.send({
            success: false,
            harvesterNotWorking: true
        });
    }

    const { success, directories } = await CoinController.getPlotsDirectories(appName);

    return res.send({
        success,
        answer: { directories }
    });
});

module.exports = { router };
