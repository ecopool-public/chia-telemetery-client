module.exports = (path) => {
    const pathPaths = path.split('/');
    const pathPathsLength = pathPaths.length;

    return {
        fileName: pathPaths[pathPathsLength - 1],
        path: pathPaths.slice(0, pathPathsLength - 1).join('/')
    };
};
