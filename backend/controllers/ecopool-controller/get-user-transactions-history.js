const socketIoClient = require('./socket-io-client');
const getLogs = require('../../utils/get-logs');
const { debugLog, debugError } = getLogs('ecopool-controller:get-user-transactions-history');

module.exports = async (payload) => {
    let requestData;

    try {
        requestData = await new Promise(resolve => {
            socketIoClient.emit('transactions-history', payload, resolve);
        });
    } catch (error) {
        debugError(JSON.stringify({
            message: 'Failed to execute user transactions history',
            error
        }));
        return { success: false };
    }

    if (!requestData.success) {
        debugError('Failed to execute output');
        return requestData;
    }

    debugLog('User transactions history get successfully');

    return {
        success: true,
        answer: requestData.answer
    };
};
