const config = require('config');
const { spawn, exec } = require('child_process');
const fs = require('fs');
const discoverCLI = require('./discover-cli');
const getPreparedPayloadForCommand = require('../../utils/get-prepared-payload-for-command');
const getLogs = require('../../utils/get-logs');
const { debugWarn, debugError } = getLogs('coin-controller:call-cli');
const state = require('../../state');

const {homedir} = require('os');

/**
 *
 * @param {string} appName
 * @param {{}} payload
 * @returns {ChildProcessWithoutNullStreams|boolean}
 */
module.exports = (appName, payload) => {
    const {
        commandPayload = {},
        customCLIPath = null,
        handler = false,
        commandForStartHarvester = false
    } = payload;
    const cliPath = ((customCLIPath ? customCLIPath : discoverCLI(appName)) || '').replace('~', homedir());

    if (!cliPath) {
        debugError(`Path to ${appName}-blockchain not found`);
        return false;
    }

    if (!state.harvesterIsWorking && !commandForStartHarvester) {
        debugWarn('Harvester not started. Command could not be executed');
        return false;
    }

    const payloadForCommand = getPreparedPayloadForCommand(commandPayload);
    const defaultCommandOptions = {
        shell: true,
        detached: true
    };
    let child;

    if (process.platform === 'win32') {
        const command = `${cliPath} ${payloadForCommand}`;

        child = exec(command, {
            ...defaultCommandOptions,
            windowsHide: true
        });
    } else {
        if (fs.existsSync(`${cliPath}/activate`)) {
            const command = `. ${cliPath}/activate && ${appName} ${payloadForCommand}`;
            child = spawn(command, defaultCommandOptions);
        } else {
            const command = `${cliPath}/${appName} ${payloadForCommand}`;
            child = spawn(command, defaultCommandOptions);
        }
    }

    const pid = child.pid;
    const logs = getLogs(`coin-controller:call-cli:${pid}`);
    const pidLog = logs.debugLog;
    const pidError = logs.debugError;
    const isDebugSpawnCommand = config.debug.spawnCommand;

    child.stderr.on('data', (data) => {
        const string = data.toString().trim();

        if (isDebugSpawnCommand) {
            pidError(string);
        }

        if (handler && typeof handler === 'function') {
            handler(child, string);
        }
    });

    child.stdout.on('data', (data) => {
        const string = data.toString().trim();

        if (isDebugSpawnCommand) {
            pidLog(string);
        }

        if (handler && typeof handler === 'function') {
            handler(child, string);
        }
    });

    child.on('exit', (exitCode) => {
        if (exitCode === 0) {
            pidLog(`Child exited with code: ${exitCode}. SUCCESS`);
        } else {
            pidError(`Child exited with code: ${exitCode}. ERROR`);
        }
    });

    return child;
};
