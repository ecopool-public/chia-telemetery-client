const router = require('express').Router();
const createError = require('http-errors');
const ROUTES_ERROR_ANSWERS = require('../../constants/routes-error-answers');
const GuiController = require('../../controllers/gui-controller');
const EcoPoolController = require('../../controllers/ecopool-controller');

router.get('/user/get-mnemonics', async (req, res, next) => {
    if (!GuiController.userIsLogged()) {
        return next(createError(401, ROUTES_ERROR_ANSWERS[401]));
    }

    const getMnemonics = await EcoPoolController.getMnemonics();

    if (getMnemonics.success) {
        return res.send({
            success: true,
            answer: getMnemonics.answer.mnemonicData
        });
    } else {
        return res.send({ success: false });
    }
});

module.exports = { router };
