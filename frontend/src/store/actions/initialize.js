import * as MutationsTypes from '../mutations/types';
import * as ActionsTypes from './types';

export default async (store) => {
    store.commit(MutationsTypes.SET_LOADING, true);

    await store.dispatch(ActionsTypes.CHECK_LOGIN);
    await store.dispatch(ActionsTypes.FETCH_NEW_VERSION);

    store.dispatch(ActionsTypes.SUBSCRIBE_HARVESTER_CONNECTION);
    store.dispatch(ActionsTypes.HARVESTER_SUBSCRIBE_PROOFS_INFO);
    store.dispatch(ActionsTypes.HARVESTER_CHART_INFO_SUBSCRIBE);

    store.commit(MutationsTypes.SET_LOADING, false);
};