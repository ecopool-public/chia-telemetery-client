module.exports = (queueData) => {
    return queueData.some(item => {
        return item.parallel.active;
    });
};
