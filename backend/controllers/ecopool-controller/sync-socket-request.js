const socketIoClient = require('./socket-io-client');
const setUser = require('./set-user');
const GuiController = require('../gui-controller');
const getLogs = require('../../utils/get-logs');
const { debugLog } = getLogs('ecopool-controller:sync-socket-request');

module.exports = async (queueName, ...args) => {
    return await new Promise(resolve => {
        debugLog(`Socket request ${queueName}, sent on server`);

        socketIoClient.emit(queueName, ...args, (payload) => {
            if (typeof payload === "undefined") {
                resolve();
            } else if (typeof payload === 'object' && payload !== null && !Array.isArray(payload)) {
                const { success, error } = payload;

                if (!success && (error && error.userId === 'not found')) {
                    setUser.mainSocket(GuiController.localConfig.get('currentUser'));
                }

                resolve(payload);
            } else {
                resolve(payload);
            }
        });
    });
};
