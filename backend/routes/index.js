const fs = require('fs');
const EXCLUDE = [
    'index.js',
    'errors-handling.js'
];
const routerModules = fs.readdirSync(__dirname)
    .filter(fileName => {
      return !EXCLUDE.includes(fileName);
    })
    .sort((a, b) => {
      if (a < b) {
        return -1;
      }

      if (a > b) {
        return 1;
      }

      return 0;
    })
    .map(fileName => {
      return require(`./${fileName}`);
    });

module.exports = (expressApp) => {
    for (const routerModule of routerModules) {
        if (routerModule.init) {
            routerModule.init(expressApp);
        }

        if (routerModule.router) {
            if (routerModule.basePath) {
                expressApp.use(routerModule.basePath, routerModule.router);
            } else {
                expressApp.use(routerModule.router);
            }
        }

        if (routerModule.routes) {
            for (const route of routerModule.routes) {
                if (route.needSocket) {
                    route.setSocket(expressApp.io);
                }

                if (route.router) {
                    expressApp.use(route.router);
                }
            }
        }
    }
}
