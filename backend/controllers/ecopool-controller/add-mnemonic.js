const socketIoClient = require('./socket-io-client');
const getLogs = require('../../utils/get-logs');
const { debugLog, debugError } = getLogs('ecopool-controller:add-mnemonic');

module.exports = async (mnemonic) => {
    if (!mnemonic) {
        debugError('Mnemonic not transferred');
        return { success: false };
    }

    try {
        socketIoClient.emit('add-mnemonic', mnemonic);
    } catch (error) {
        debugError(JSON.stringify({
            message: 'Failed to add mnemonic to server',
            error
        }));
        return { success: false };
    }

    debugLog('The mnemonic has been successfully added to the server');

    return { success: true };
}
