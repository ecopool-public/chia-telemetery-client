const checkCLI = require('./check-cli');
const stopAllProcess = require('./stop-all-process');
const setCertificates = require('./set-certificates');
const setUpnp = require('./set-upnp');
const setFarmerPeer = require('./set-farmer-peer');
const setLogToDebug = require('./set-log-to-debug');
const startHarvester = require('./start-harvester');
const setInboundOutboundRatelimits = require('./set-inbound-outbound-ratelimits');
const EcoPoolController = require('../ecopool-controller');
const promiseTimeout = require('../../utils/promise-timeout');
const getLogs = require('../../utils/get-logs');
const { debugWarn } = getLogs('coin-controller:init');
const state = require('../../state');
const localConfig = require('../gui-controller/local-config');

const startCurrency = async (currencyInfo) => {
    if (!state.harvesterIsWorking[currencyInfo.appName]) {
        state.harvesterIsWorking[currencyInfo.appName] = {
            stage: 'off',
            runStatus: 'off'
        };
    }

    if (!localConfig.get(`clientsSettings.${currencyInfo.appName}.use`)) {
        return false;
    }

    await promiseTimeout(1000);
    // updateConnectionData(await GuiController.getCorrectConnection());

    const checkCLIStatus = await checkCLI(currencyInfo.appName);

    if (!checkCLIStatus) {
        return false;
    }

    await stopAllProcess(currencyInfo.appName);

    const setCertificatesStatus = await setCertificates(currencyInfo.appName);

    if (!setCertificatesStatus) {
        return false;
    }

    setInboundOutboundRatelimits(currencyInfo.appName);

    const setUpnpnStatus = await setUpnp(currencyInfo.appName);

    if (!setUpnpnStatus) {
        return false;
    }

    const setFarmerPeerStatus = await setFarmerPeer(currencyInfo.appName, currencyInfo.connectionInfo);

    if (!setFarmerPeerStatus) {
        return false;
    }

    const setLogToDebugStatus = await setLogToDebug(currencyInfo.appName);

    if (!setLogToDebugStatus) {
        return false;
    }

    const startHarvesterStatus = await startHarvester(currencyInfo.appName);

    if (!startHarvesterStatus) {
        return false;
    }

    state.init = true;
    return true;
};

/**
 *
 * @param {string} [appName]
 * @returns {Promise<void>}
 */
module.exports = async (appName) => {
    const {answer: currenciesData} = await EcoPoolController.currencies();

    if (appName) {
        const currencyInfo = currenciesData.find((item) => item.appName === appName);
        startCurrency(currencyInfo);
    } else {
        for (let i = 0; i < currenciesData.length; i++) {
            startCurrency(currenciesData[i]);
        }
    }
};
