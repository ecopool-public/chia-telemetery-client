const ccxt = require('ccxt');
const {io} = require('../backend/app');

const lbank = new ccxt.lbank();
let LAST_TICKERS_DATA = {};

setInterval(async () => {
    LAST_TICKERS_DATA = await lbank.fetchTickers();

    io.emit('currencies-prices', LAST_TICKERS_DATA);
}, 30000);

module.exports = (currency) => {
    const ticker = `${currency.toUpperCase()}/USDT`;

    return (LAST_TICKERS_DATA[ticker] || {}).last || 0;
}