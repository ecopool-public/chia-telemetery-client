const config = require('config');
const timeRegex = /^(\d{4}-\d{2}-\d{2}T\d{2}:\d{2}:\d{2}\.\d{3})/;

module.exports = (logString) => {
    if (logString.indexOf(`farming_info to peer ${config.pool.ip}`) === -1) {
        return false;
    }

    const time = logString.match(timeRegex);

    if (!time) {
        return false;
    }

    return time[0];
};
