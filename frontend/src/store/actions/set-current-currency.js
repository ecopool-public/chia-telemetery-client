import * as MutationsTypes from '../mutations/types';
import io from '../../sockets';

export default async (store, currency) => {
    store.commit(MutationsTypes.SET_CURRENT_CURRENCY, currency);
    store.commit(MutationsTypes.SET_CURRENCY_PRICE, 0);

    io.emit('get-currency-price', store.state.currencyId, (price) => {
        store.commit(MutationsTypes.SET_CURRENCY_PRICE, price);
    });

    io.on('currencies-prices', () => {
        io.emit('get-currency-price', store.state.currencyId, (price) => {
            store.commit(MutationsTypes.SET_CURRENCY_PRICE, price);
        });
    });
};