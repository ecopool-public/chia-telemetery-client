const path = require('path');
const { homedir } = require('os');
const fs = require('fs');
const getHash = require('../../utils/get-hash');
const getLogs = require('../../utils/get-logs');
const EcoPoolController = require("../../controllers/ecopool-controller");
const { debugError } = getLogs('coin-controller:get-harvester-id');

let LAST_HARVESTER_ID = null;

/**
 * @param {string} appName
 * @returns {string}
 */
const getPathToCert = (appName) => {
    return path.resolve(homedir(), `./.${appName}/mainnet/config/ssl/harvester/private_harvester.crt`);
};

/**
 * @param {string} appName
 * @returns {string|null}
 */
module.exports = (appName) => {
    const pathToCert = getPathToCert(appName);

    try {
        const certificate = fs.readFileSync(pathToCert).toString()
            .split('\n')
            .filter(line => !line.includes('-----'))
            .map(line => line.trim() )
            .join('');

        const harvesterId = getHash(certificate, 'base64', 'hex');

        (async () => {
            if (harvesterId !== LAST_HARVESTER_ID) {
                EcoPoolController.socketIoClientForSendPlotsOnServer.emit('check-harvester-id', harvesterId, (result) => {
                    if (result) {
                        LAST_HARVESTER_ID = harvesterId;
                    }
                });
            }
        })();

        return harvesterId;
    } catch (error) {
        debugError(JSON.stringify({
            message: `Can\`t read harvester certificate for ${appName}`,
            error
        }));
        return null;
    }
};
