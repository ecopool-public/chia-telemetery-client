export default (state, {appName, payload}) => {
    state.harvesterChartInfo[appName] = payload.reverse();
};