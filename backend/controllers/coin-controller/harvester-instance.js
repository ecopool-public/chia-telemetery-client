const {getChiaConfig} = require('chia-client/dist/src/ChiaNodeUtils');
require('chia-client/dist/src/ChiaNodeUtils').getChiaConfig = (...args) => {
    try {
        return getChiaConfig(...args);
    } catch (e) {
        return {
            full_node: {},
            private_ssl_ca: {},
            daemon_ssl: {},
            harvester: {}
        };
    }
}

const { Harvester } = require('chia-client/dist/src/Harvester');

const EcopoolController = require('../ecopool-controller');
const {homedir} = require("os");
const fs = require("fs");

const HARVESTERS_CACHE = {};

const prepareSettingsWithCertsPaths = (settings) => {
    return Object.keys(settings)
        .reduce((res, key) => {
            if (key.indexOf('_path') !== -1) {
                const keyVal = settings[key].replace('~', homedir());
                res[key.replace('_path', '')] = fs.readFileSync(keyVal);
            } else if (key.indexOf('Path') !== -1) {
                const keyVal = settings[key].replace('~', homedir());
                res[key] = keyVal;
            } else {
                res[key] = settings[key];
            }

            return res;
        }, {});
};

module.exports = async (appName) => {
    if (!HARVESTERS_CACHE[appName]) {
        const {answer: currencies} = await EcopoolController.currencies();

        const currencyInstance = currencies.find((item) => item.appName === appName);

        HARVESTERS_CACHE[appName] = new Harvester(
            prepareSettingsWithCertsPaths(currencyInstance.harvesterConnectionSettings)
        );
    }
    return HARVESTERS_CACHE[appName];
};
