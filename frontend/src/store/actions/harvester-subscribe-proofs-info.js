import axios from "axios";
import socket from "../../sockets";

import * as MutationsTypes from '../mutations/types';

let SUBSCRIBED = false;

export default async (store) => {
    if (SUBSCRIBED) {
        return;
    }

    SUBSCRIBED = true;

    let canAppendData = false;

    axios.get('/api/harvester/get-telemetry-history')
        .then((resp) => {
            if (resp.data.success) {
                // this.setTableData(resp.data.answer.history);
                const history = resp.data.answer.history;

                Object.keys(history)
                    .forEach((appName) => {
                        const localHistory = history[appName]
                            .reverse()
                            .slice(0,20);

                        localHistory.forEach((item) => {
                            store.commit(MutationsTypes.HARVESTER_PROOFS_INFO_APPEND, {
                                appName,
                                filter: item
                            });
                        })
                    })
            }
        })
        .finally(() => canAppendData = true);

    socket.on('harvester-telemetry', (appName, payload) => {
        // this.setTableDataOnSockets(payload.filter);
        if (!canAppendData) {
            return;
        }

        store.commit(MutationsTypes.HARVESTER_PROOFS_INFO_APPEND, {
            appName,
            ...payload
        });
    })
};