import * as MutationsTypes from '../mutations/types';
import * as ActionsTypes from './types';
import axios from "axios";
import ElNotification from "element-plus/lib/el-notification";

export default async (store, pathToRemove) => {
    store.commit(MutationsTypes.SET_LOADING, true);

    try {
        let resp = await axios.post(`/api/harvester/remove-plots-directory/${store.state.appName}`, {
            path: pathToRemove
        });
        if (resp.data.success) {
            ElNotification.success({
                title: 'Success',
                message: 'Directory was removed',
            });

            await store.dispatch(ActionsTypes.PLOT_DIRECTORIES_FETCH);

            return true
        } else {
            ElNotification.error({
                title: 'Error',
                message: "Couldn't remove a plot directory",
            });

            return false;
        }
    } catch(error) {
        console.error(error);
        return false;
    }
};
