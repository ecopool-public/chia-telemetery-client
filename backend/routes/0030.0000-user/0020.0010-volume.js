const router = require('express').Router();
const createError = require('http-errors');
const ROUTES_ERROR_ANSWERS = require('../../constants/routes-error-answers');
const GuiController = require('../../controllers/gui-controller');
const EcoPoolController = require('../../controllers/ecopool-controller');

router.get('/user/volume/:currencyId', async (req, res, next) => {
    if (!GuiController.userIsLogged()) {
        return next(createError(401, ROUTES_ERROR_ANSWERS[401]));
    }

    const {currencyId} = req.params;

    let poolVolumeData;
    let userVolumeData;

    try {
        poolVolumeData = await EcoPoolController.poolVolume(currencyId);
    } catch (error) {
        return res.send({ success: false });
    }

    try {
        userVolumeData = await EcoPoolController.userVolume(currencyId);
    } catch (error) {
        return res.send({ success: false });
    }

    if (!poolVolumeData.success || !userVolumeData.success) {
        return res.send({ success: false });
    }

    return res.send({
        success: true,
        answer: {
            pool: poolVolumeData.answer.volume,
            user: userVolumeData.answer.volume,
            totalPlotsCount: 0
        }
    });
});

module.exports = { router };
