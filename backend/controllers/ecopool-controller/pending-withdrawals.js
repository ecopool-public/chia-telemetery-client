const syncSocketRequest = require('./sync-socket-request');
const getLogs = require('../../utils/get-logs');
const { debugLog, debugError } = getLogs('ecopool-controller:pending-withdrawals');

module.exports = async (currencyId) => {
    let requestData;

    try {
        requestData = syncSocketRequest('pending-withdrawals', currencyId);
    } catch (error) {
        debugError(JSON.stringify({
            message: 'Failed to complete the request to write off the balance',
            error
        }));
        return { success: false };
    }

    const {
        success,
        answer
    } = requestData;

    if (!success) {
        return requestData;
    }

    debugLog('The balance was written off successfully');

    return {
        success: true,
        answer
    };
};
