#!/bin/bash

#stop client
pm2 stop all

#stop chia
cd ~/chia-blockchain/
. ./activate
chia stop all -d
chia stop all
deactivate

#update client
cd ~/ecopool-client/
git reset --hard
git pull
yarn install

#restart client
pm2 restart all
echo "wait 30 sec."
sleep 30
cd ~/chia-blockchain/
. ./activate
chia start harvester -r
deactivate

#chmod exec
cd ~/ecopool-client/
chmod +x update-client.sh

####pm2 log 0
echo "update completed!"
