const startHarvester = require('./start-harvester');
const getLogs = require('../../utils/get-logs');
const { debugLog, debugError } = getLogs('coin-controller:restart-harvester');
const state = require('../../state');

/**
 * @param {string} appName
 * @returns {Promise<*>}
 */
module.exports = (appName) => {
    debugLog(`Harvester restart started for ${appName}`);
    state.harvesterIsWorking = false;

    const restartHarvesterStatus = startHarvester(appName);

    if (restartHarvesterStatus) {
        debugLog(`Harvester restart completed successfully for ${appName}`);
    } else {
        debugError(`An error occurred when restarting the harvester for ${appName}`);
    }

    return restartHarvesterStatus;
};
